﻿using BooleanLogicSim.Circuit.Logic.Components;
using BooleanLogicSim.Data;
using BooleanLogicSim.Model;
using BooleanLogicSim.Model.BooleanAlgebra;
using Microsoft.VisualBasic;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using static BooleanLogicSim.IO.ResultIO;

namespace BooleanLogicSim.IO
{
    public static class EvaluationIO
    {
        public readonly static string[] ValidBooleanInputValues = new string[] { "true", "false", "1", "0" };

        public static Dictionary<string, bool> GetInputsForExpression(BooleanExpression expression)
        {
            var inputs = new Dictionary<string, bool>();

            foreach (var inputSymbol in expression.InputSymbols)
            {
                var inputValue = string.Empty;

                while (string.IsNullOrWhiteSpace(inputValue))
                {
                    var userInput = Interaction.InputBox($"Enter a value for input {inputSymbol} (true, false, 0, 1): ", "Input Value", "").ToLower();

                    if (userInput.Length == 0) return null;

                    if (!ValidBooleanInputValues.Contains(userInput))
                    {
                        ShowInputError("That isn't a valid input value.");
                        inputValue = string.Empty;

                        continue;
                    }

                    inputValue = userInput;
                }

                if (inputValue == "true" || inputValue == "1")
                {
                    inputs[inputSymbol] = true;
                }
                else
                {
                    inputs[inputSymbol] = false;
                }
            }

            return inputs;
        }
    }
}
