﻿using BooleanLogicSim.Circuit.Logic.Components;
using BooleanLogicSim.Data;
using BooleanLogicSim.Model.BooleanAlgebra;
using Microsoft.VisualBasic;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BooleanLogicSim.IO.ResultIO;

namespace BooleanLogicSim.IO
{
    public static class ComponentsIO
    {
        public static string GetComponentPlainName(IComponentRepository componentRepository)
        {
            var plainName = string.Empty;

            while (string.IsNullOrWhiteSpace(plainName))
            {
                plainName = Interaction.InputBox("Enter the plain name for this component (e.g. AND, OR, XOR)", "Plain Name");

                if (plainName.Length == 0) return string.Empty;

                if (componentRepository.GetComponentByName(plainName) != null)
                {
                    ShowIOError("A component with that name already exists.");
                    plainName = "";

                    continue;
                }
            }

            return plainName;
        }

        public static string GetComponentSymbol(IComponentRepository componentRepository)
        {
            var symbol = string.Empty;

            while (string.IsNullOrEmpty(symbol))
            {
                symbol = Interaction.InputBox("Enter the symbol for this component (e.g. @, #, /)", "Algebra Symbol");

                if (symbol.Length == 0) return string.Empty;

                if (symbol.Length > 1)
                {
                    ShowInputError("A symbol must be a single character.");
                    symbol = "";

                    continue;
                }

                if (!(componentRepository.GetComponentBySymbol(symbol) is Input))
                {
                    ShowInputError("A component with this symbol already exists.");
                    symbol = "";

                    continue;
                }

                if (TokenTypes.Tokens.Keys.Contains(symbol))
                {
                    ShowInputError("A token with this symbol already exists.");
                    symbol = "";

                    continue;
                }
            }

            return symbol;
        }

        public static string GetComponentImageFilePath()
        {
            var filePath = string.Empty;

            while (string.IsNullOrEmpty(filePath))
            {
                try
                {
                    var fileDialog = new OpenFileDialog()
                    {
                        Filter = "Image Files (*.bmp;*.jpg;*.jpeg;*.gif;*.png)|*.bmp;*.jpg;*.jpeg;*.gif;*.png",
                        CheckFileExists = true,
                        CheckPathExists = true
                    };

                    if (fileDialog.ShowDialog() != true) return string.Empty;

                    filePath = fileDialog.FileName;
                }
                catch (IOException ex)
                {
                    ShowIOError($"There was an error reading the file. ({ex.Message})");
                    filePath = string.Empty;

                    continue;
                }
            }

            return filePath;
        }
    }
}
