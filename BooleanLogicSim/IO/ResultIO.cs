﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BooleanLogicSim.IO
{
    public static class ResultIO
    {
        public static void ShowResult(string result)
        {
            MessageBox.Show(result, "Result", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public static void ShowConversionError(string error)
        {
            MessageBox.Show(error, "Conversion Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        public static void ShowProgrammingError(string error)
        {
            MessageBox.Show(error, "Programming Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        public static void ShowCircuitError(string error)
        {
            MessageBox.Show(error, "Circuit Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        public static void ShowIOError(string error)
        {
            MessageBox.Show(error, "File IO Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        public static void ShowInputError(string error)
        {
            MessageBox.Show(error, "Invalid Input Value", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }
    }
}
