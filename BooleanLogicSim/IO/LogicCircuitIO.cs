﻿using BooleanLogicSim.Model;
using BooleanLogicSim.Model.LogicCircuits;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSim.IO
{
    public static class LogicCircuitIO
    {
        public static Result<bool> SaveCircuit(string fileName, LogicCircuit logicCircuit)
        {
            try
            {
                using (var file = File.Create(fileName))
                {
                    var serializer = new BinaryFormatter();
                    serializer.Serialize(file, logicCircuit);
                }

                return new Result<bool>(true);
            }
            catch (IOException ex)
            {
                return new Result<bool>($"There was an error writing the file. ({ex.Message})");
            }
        }

        public static Result<LogicCircuit> LoadCircuit(string fileName)
        {
            try
            {
                LogicCircuit logicCircuit;

                using (var file = File.OpenRead(fileName))
                {
                    var deserializer = new BinaryFormatter();
                    logicCircuit = (LogicCircuit)deserializer.Deserialize(file);
                }

                return new Result<LogicCircuit>(logicCircuit);
            }
            catch (IOException ex)
            {
                return new Result<LogicCircuit>($"There was an error reading the file. ({ex.Message})");
            }
        }
    }
}
