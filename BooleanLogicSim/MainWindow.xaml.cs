﻿using BooleanLogicSim.CanvasElements;
using BooleanLogicSim.Circuit;
using BooleanLogicSim.Data;
using BooleanLogicSim.View;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Serialization;
using System.Xml;
using BooleanLogicSim.Model;
using BooleanLogicSim.Model.LogicCircuits;
using BooleanLogicSim.Model.BooleanAlgebra;
using Microsoft.VisualBasic;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.Win32;
using BooleanLogicSim.Circuit.Logic.Components;
using BooleanLogicSim.Circuit.View.Canvas;
using BooleanLogicSim.Circuit.Logic;
using static BooleanLogicSim.Circuit.Logic.Components.CustomComponent;
using static BooleanLogicSim.IO.EvaluationIO;
using static BooleanLogicSim.IO.ResultIO;
using static BooleanLogicSim.IO.ComponentsIO;
using System.Diagnostics;
using BooleanLogicSim.IO;

namespace BooleanLogicSim
{
    public partial class MainWindow : Window
    {
        public static string ComponentListBoxDataIdentifier { get; } = "ComponentListBoxItem";

        private CircuitCanvas CircuitCanvas { get; }

        private CustomComponentRepository CustomComponentRepository { get; }

        private IComponentRepository ComponentRepository { get; }

        private LogicCircuit _logicCircuit = new LogicCircuit();

        private LogicCircuit LogicCircuit
        {
            get => _logicCircuit;

            set
            {
                _logicCircuit = value;
                this.CircuitCanvas.LogicCircuit = value;
            }
        }


        public ComponentsListBoxViewModel ComponentsListBoxViewModel { get; }

        public int RecommendedMaxNumberOfVariables { get; private set; } = 6;

        public bool VariableNumberLimitEnabled => VariableNumberLimitMenuItem.IsChecked;

        public MainWindow()
        {
            InitializeComponent();

            var customCompRepoResult = CustomComponentRepository.LoadComponentsRepo();
            if (customCompRepoResult.IsError)
            {
                ShowIOError(customCompRepoResult.Error);
                this.CustomComponentRepository = new CustomComponentRepository();
            }
            else
            {
                this.CustomComponentRepository = customCompRepoResult.Value;
            }

            ComponentRepository = new DefaultComponentRepository(CustomComponentRepository);

            ComponentsListBoxViewModel = new ComponentsListBoxViewModel(ComponentRepository);
            ComponentsListBox.DataContext = ComponentsListBoxViewModel;

            CircuitCanvas = new CircuitCanvas(this, MainCanvas, ComponentRepository, LogicCircuit);
        }

        private void Menu_File_Save_Circuit_Click(object sender, ExecutedRoutedEventArgs e)
        {
            var fileDialog = new SaveFileDialog()
            {
                Filter = "Circuit Files (*.blsc)|*.blsc",
                CheckPathExists = true
            };

            if (fileDialog.ShowDialog() != true) return;

            foreach (var circuitComp in MainCanvas.Children.OfType<CircuitCanvasComponent>())
                circuitComp.Component.ComponentView.Position = new Point(Canvas.GetLeft(circuitComp), Canvas.GetTop(circuitComp));

            var saveResult = LogicCircuitIO.SaveCircuit(fileDialog.FileName, this.LogicCircuit);
            if (saveResult.IsError)
            {
                ShowIOError(saveResult.Error);
            }
        }

        private void Menu_File_Load_Circuit_Click(object sender, ExecutedRoutedEventArgs e)
        {
            var fileDialog = new OpenFileDialog()
            {
                Filter = "Circuit Files (*.blsc)|*.blsc",
                CheckPathExists = true
            };

            if (fileDialog.ShowDialog() != true) return;

            var logicCircuitResult = LogicCircuitIO.LoadCircuit(fileDialog.FileName);
            if (logicCircuitResult.IsError)
            {
                ShowIOError(logicCircuitResult.Error);
                return;
            }

            var logicCircuit = logicCircuitResult.Value;

            var customComponents = logicCircuit.InnerComponents.OfType<CustomComponent>().ToList();

            if (customComponents.Count > 0)
            {
                var addResult = AddCustomComponents(customComponents);
                if (addResult.IsError)
                {
                    ShowResult(addResult.Error);
                    return;
                }

                var circuitSaveResult = LogicCircuitIO.SaveCircuit(fileDialog.FileName, logicCircuit);
                if (circuitSaveResult.IsError) ShowIOError(circuitSaveResult.Error);
            }

            this.LogicCircuit = logicCircuit;
        }

        private Result<bool> AddCustomComponents(List<CustomComponent> customComponents)
        {
            var addResult = this.CustomComponentRepository.AddComponents(customComponents);
            if (addResult.IsError) return addResult;

            this.CustomComponentRepository.UpdateCustomComponentPaths(customComponents);

            this.ComponentsListBoxViewModel.UpdateComponents();

            var repoSaveResult = this.CustomComponentRepository.SaveComponentsRepo();
            if (repoSaveResult.IsError) return new Result<bool>(repoSaveResult.Error);

            return new Result<bool>(true);
        }

        private void Menu_File_Export_Component_Click(object sender, RoutedEventArgs e)
        {
            if (this.LogicCircuit.Inputs.Count != 2 || this.LogicCircuit.Output == null)
            {
                ShowCircuitError("Custom components must have only 2 inputs and an output.");
                return;
            }

            var validationResult = this.LogicCircuit.EvaluateCircuit();
            if (validationResult.IsError)
            {
                ShowCircuitError(validationResult.Error);
                return;
            }

            var plainName = GetComponentPlainName(this.ComponentRepository);
            if (string.IsNullOrWhiteSpace(plainName)) return;

            var symbol = GetComponentSymbol(this.ComponentRepository);
            if (string.IsNullOrWhiteSpace(symbol)) return;

            var filePath = GetComponentImageFilePath();
            if (string.IsNullOrWhiteSpace(filePath)) return;

            var customComponent = new CustomComponent(this.LogicCircuit, plainName, symbol, filePath);

            this.CustomComponentRepository.AddComponent(customComponent);
            this.ComponentsListBoxViewModel.UpdateComponents();

            this.LogicCircuit = new LogicCircuit();

            var canvasComponent = new CircuitCanvasComponent();
            canvasComponent.SetComponent(customComponent);
            canvasComponent.SetLogicCircuit(this.LogicCircuit);
            canvasComponent.SetMainWindow(this);

            this.CircuitCanvas.AddCanvasElement(canvasComponent);

            this.EnableOutputCreation();

            var saveResult = this.CustomComponentRepository.SaveComponentsRepo();
            if (saveResult.IsError)
            {
                ShowIOError(saveResult.Error);
            }
        }

        private void Menu_File_Export_Canvas_PNG_Click(object sender, RoutedEventArgs e)
        {
            var fileDialog = new SaveFileDialog()
            {
                Filter = "PNG Files (*.png)|*.png",
                CheckPathExists = true
            };

            if (fileDialog.ShowDialog() != true) return;

            var filePath = fileDialog.FileName;

            var width = MainCanvas.Children.OfType<CircuitCanvasComponent>().Max(comp => Canvas.GetLeft(comp));
            var height = MainCanvas.Children.OfType<CircuitCanvasComponent>().Max(comp => Canvas.GetTop(comp));

            var renderBitmap = new RenderTargetBitmap((int)width + 150, (int)height + 100, 96, 96, PixelFormats.Pbgra32);

            MainCanvas.Measure(new Size(width, height));
            MainCanvas.Arrange(new Rect(new Size(width, height)));

            renderBitmap.Render(MainCanvas);

            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(renderBitmap));

            try
            {
                using (var file = File.Create(filePath))
                {
                    encoder.Save(file);
                }
            }
            catch (IOException ex)
            {
                ShowIOError($"There was an error saving the file. ({ex.Message})");
            }
        }

        private void Menu_File_Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Menu_About_Contributions_Click(object sender, RoutedEventArgs e)
        {
            var contributionsMessage = "Icons from https://icons8.com/";

            MessageBox.Show(contributionsMessage, "Contributions", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void ComponentsListBoxElement_MouseDown(object sender, MouseEventArgs e)
        {
            var listBoxItem = sender as ListBoxItem;
            var component = listBoxItem.DataContext as IComponent;

            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var data = new DataObject(ComponentListBoxDataIdentifier, component.ComponentView.PlainName);

                DragDrop.DoDragDrop((DependencyObject)sender, data, DragDropEffects.Copy);

                if (component.ComponentView.PlainName == Output.ComponentName && this.LogicCircuit.Output != null)
                {
                    this.DisableOutputCreation();
                }
            }
        }

        public void EnableOutputCreation()
        {
            foreach (IComponent itemComp in ComponentsListBox.Items)
            {
                var item = ComponentsListBox.ItemContainerGenerator.ContainerFromItem(itemComp) as ListBoxItem;
                if (item == null)
                {
                    ComponentsListBox.UpdateLayout();
                    item = ComponentsListBox.ItemContainerGenerator.ContainerFromItem(itemComp) as ListBoxItem;
                }

                var componentName = (item.DataContext as IComponent).ComponentView.PlainName;

                if (componentName == Output.ComponentName)
                {
                    item.IsEnabled = true;
                }
            }
        }

        public void DisableOutputCreation()
        {
            foreach (IComponent itemComp in ComponentsListBox.Items)
            {
                var item = ComponentsListBox.ItemContainerGenerator.ContainerFromItem(itemComp) as ListBoxItem;
                if (item == null)
                {
                    ComponentsListBox.UpdateLayout();
                    item = ComponentsListBox.ItemContainerGenerator.ContainerFromItem(itemComp) as ListBoxItem;
                }

                var componentName = (item.DataContext as IComponent).ComponentView.PlainName;

                if (componentName == Output.ComponentName)
                {
                    item.IsEnabled = false;
                }
            }
        }

        private void EvaluateCircuitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var result = LogicCircuit.EvaluateCircuit();
                if (result.IsError)
                {
                    ShowCircuitError(result.Error);
                    return;
                }

                ShowResult($"Circuit output: {result.Value}");
            }
            catch (Exception ex)
            {
                ShowProgrammingError($"There was an unintended error evaluating the circuit. ({ex.Message})");
            }
        }

        private void CircuitToExpressionConvertButton_Click(object sender, RoutedEventArgs e)
        {
            var expression = LogicCircuit.ConstructBooleanAlgebraInfixExpression();
            if (expression.IsError)
            {
                ShowConversionError(expression.Error);
                return;
            }

            ExpressionTextBox.Text = expression.Value;
        }

        private void ExpressionToCircuitConvertButton_Click(object sender, RoutedEventArgs e)
        {
            var expressionResult = BooleanExpression.FromInfixString(ExpressionTextBox.Text, this.ComponentRepository);
            if (expressionResult.IsError)
            {
                ShowConversionError($"There was an error converting the expression:\n{expressionResult.Error}");
                return;
            }

            var expression = expressionResult.Value;

            this.LogicCircuit = expression.EquivalentLogicCircuit;
        }

        private void EvaluateExpressionButton_Click(object sender, RoutedEventArgs e)
        {
            var expressionResult = BooleanExpression.FromInfixString(ExpressionTextBox.Text, this.ComponentRepository);
            if (expressionResult.IsError)
            {
                ShowConversionError($"There was an error converting the expression:\n{expressionResult.Error}");
                return;
            }

            var expression = expressionResult.Value;

            var inputs = GetInputsForExpression(expression);
            if (inputs == null) return;

            var evaluationResult = expression.EvaluateExpression(inputs);
            if (evaluationResult.IsError)
            {
                ShowCircuitError(evaluationResult.Error);
                return;
            }

            ShowResult($"Evaluation result: {evaluationResult.Value}");
        }

        private void SimplifyExpressionButton_Click(object sender, RoutedEventArgs e)
        {
            var expressionResult = BooleanExpression.FromInfixString(ExpressionTextBox.Text, this.ComponentRepository);
            if (expressionResult.IsError)
            {
                ShowConversionError(expressionResult.Error);
                return;
            }

            var expression = expressionResult.Value;

            if (expression.InputSymbols.Count > RecommendedMaxNumberOfVariables && VariableNumberLimitEnabled)
            {
                var result = MessageBox.Show($"This expression contains over {RecommendedMaxNumberOfVariables} variable(s) and may take a while to process. Are you sure you want to simplify this expression?", "Confirm Simplification", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                if (result != MessageBoxResult.Yes) return;
            }

            var simplifiedResult = expression.SimplifyExpression(CustomComponentSimplificationMenuItem.IsChecked);

            if (simplifiedResult.IsError)
            {
                ShowResult(simplifiedResult.Error);
                return;
            }

            var simplified = simplifiedResult.Value;

            ExpressionTextBox.Text = simplified.InfixExpression;
        }

        private void SimplifyCircuitButton_Click(object sender, RoutedEventArgs e)
        {
            var expressionResult = this.LogicCircuit.ConstructBooleanAlgebraInfixExpression()
                                                    .FlatMap(exprText => BooleanExpression.FromInfixString(exprText, this.ComponentRepository));

            if (expressionResult.IsError)
            {
                ShowConversionError(expressionResult.Error);
                return;
            }

            var expression = expressionResult.Value;

            if (expression.InputSymbols.Count > RecommendedMaxNumberOfVariables && VariableNumberLimitEnabled)
            {
                var result = MessageBox.Show($"This circuit contains over {RecommendedMaxNumberOfVariables} input(s) and may take a while to process. Are you sure you want to simplify this circuit?", "Confirm Simplification", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                if (result != MessageBoxResult.Yes) return;
            }

            var simplifiedResult = expression.SimplifyExpression(CustomComponentSimplificationMenuItem.IsChecked);

            if (simplifiedResult.IsError)
            {
                ShowResult(simplifiedResult.Error);
                return;
            }

            var simplified = simplifiedResult.Value;

            this.LogicCircuit = simplified.EquivalentLogicCircuit;
        }

        private void ResetCircuitViewButton_Click(object sender, RoutedEventArgs e)
        {
            CircuitCanvas.ResetView();
        }

        private void ClearCircuitButton_Click(object sender, RoutedEventArgs e)
        {
            var result = MessageBox.Show("Are you sure you want to clear this circuit?", "Confirm clear", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);

            if (result != MessageBoxResult.Yes) return;

            this.LogicCircuit = new LogicCircuit();

            this.EnableOutputCreation();
        }

        private void ComponentsListBoxItem_RemoveComponent_MenuItem_Click(object sender, ExecutedRoutedEventArgs e)
        {
            var componentToRemove = (e.OriginalSource as ListBoxItem).DataContext as CustomComponent;
            if (componentToRemove == null)
            {
                ShowResult("You cannot remove built-in components!");
                return;
            }

            this.CustomComponentRepository.RemoveComponentByName(componentToRemove.ComponentView.PlainName);
            this.ComponentsListBoxViewModel.UpdateComponents();

            var toRemove = new List<CircuitCanvasComponent>();

            foreach (UIElement comp in MainCanvas.Children)
            {
                if (comp is CircuitCanvasComponent canvasComp && canvasComp.Component.ComponentView.PlainName == componentToRemove.ComponentView.PlainName)
                {
                    toRemove.Add(canvasComp);
                }
            }

            toRemove.ForEach(canvasComp => canvasComp.DeleteComponent());
        }

        private void ComponentsListBoxItem_UpdateImageFilePath_MenuItem_Click(object sender, ExecutedRoutedEventArgs e)
        {
            var componentToUpdate = (e.OriginalSource as ListBoxItem).DataContext as CustomComponent;
            if (componentToUpdate == null)
            {
                ShowResult("You cannot change built-in components!");
                return;
            }

            var filePath = GetComponentImageFilePath();
            if (string.IsNullOrWhiteSpace(filePath)) return;

            this.CustomComponentRepository.UpdateImageFilePathByName(componentToUpdate.ComponentView.PlainName, filePath);

            CircuitCanvas.UpdateCustomComponentImages(componentToUpdate);
        }
    }
}
