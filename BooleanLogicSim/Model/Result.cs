﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSim.Model
{
    public class Result<T>
    {
        public T Value { get; }

        public string Error { get; }
        public bool IsError { get; }

        public Result(string error)
        {
            this.Error = error;
            this.IsError = true;
        }

        public Result(T value)
        {
            this.Value = value;
            this.IsError = false;
        }

        public Result<V> FlatMap<V>(Func<T, Result<V>> func)
        {
            if (this.IsError)
            {
                return new Result<V>(this.Error);
            }
            else
            {
                return func(this.Value);
            }
        }

        public Result<V> FMap<V>(Func<T, V> func) => FlatMap(t => new Result<V>(func.Invoke(t)));
    }
}
