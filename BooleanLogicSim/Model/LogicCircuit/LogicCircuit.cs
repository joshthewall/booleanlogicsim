﻿using BooleanLogicSim.Circuit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Windows;
using BooleanLogicSim.Model.BooleanAlgebra;
using BooleanLogicSim.Circuit.Logic;
using BooleanLogicSim.Circuit.Logic.Components;
using System.IO;
using Microsoft.Win32;
using System.Runtime.Serialization.Formatters.Binary;
using BooleanLogicSim.Data;
using static BooleanLogicSim.Circuit.Logic.Components.CustomComponent;
using BooleanLogicSim.Extensions;

namespace BooleanLogicSim.Model.LogicCircuits
{
    [Serializable()]
    public class LogicCircuit
    {
        public List<Input> Inputs { get; private set; } = new List<Input>();
        public List<ICircuitComponent> InnerComponents { get; private set; } = new List<ICircuitComponent>();
        public Output Output { get; private set; }

        public bool AddComponent(ICircuitComponent component)
        {
            if (component is Input input)
            {
                return AddComponent(input);
            }
            else if (component is Output output)
            {
                return AddComponent(output);
            }
            else
            {
                if (InnerComponents.Contains(component)) return true;

                InnerComponents.Add(component);
            }

            return true;
        }

        private bool AddComponent(Input input)
        {
            if (Inputs.Contains(input)) return true;

            Inputs.Add(input);

            return true;
        }

        private bool AddComponent(Output output)
        {
            if (Output != null) return false;

            Output = output;

            return true;
        }

        public bool RemoveComponent(ICircuitComponent component)
        {
            if (component is Input input)
            {
                return Inputs.Remove(input);
            }
            else if (component is Output)
            {
                if (component != Output) return false;

                Output = null;

                return true;
            }
            else
            {
                return InnerComponents.Remove(component);
            }
        }

        public Result<int> GetMaximumDepth()
        {
            var rootNodeResult = ConstructBinaryExpressionTree();
            if (rootNodeResult.IsError) return new Result<int>(rootNodeResult.Error);

            var rootNode = rootNodeResult.Value;

            var maximumDepth = MaximumDepth(rootNode);

            return new Result<int>(maximumDepth);
        }

        private int MaximumDepth(TreeNode<ICircuitComponent> rootNode)
        {
            if (rootNode == null) return 0;

            var leftMax = MaximumDepth(rootNode.Left);
            var rightMax = MaximumDepth(rootNode.Right);

            var maxDepth = Math.Max(leftMax, rightMax) + 1;

            return maxDepth;
        }

        public Result<bool> EvaluateCircuit()
        {
            var rootNodeResult = ConstructBinaryExpressionTree();
            if (rootNodeResult.IsError) return new Result<bool>(rootNodeResult.Error);

            var rootNode = rootNodeResult.Value;

            var booleanResult = EvaluateTree(rootNode);
            if (booleanResult == null) return new Result<bool>("Circuit evaluation failed. The circuit may be empty or a component is missing inputs.");

            return new Result<bool>(booleanResult.Value);
        }

        public Result<bool> EvaluateCircuit(Dictionary<string, bool> inputs)
        {
            if (!this.Inputs.All(input => inputs.ContainsKey(input.ComponentView.AlgebraLabel)))
            {
                return new Result<bool>("The inputs given do not cover the inputs of the expression.");
            }

            var originalInputs = this.Inputs.Select(input => input.Value).ToList();

            this.Inputs.ForEach(input => input.Value = inputs[input.ComponentView.AlgebraLabel]);

            var evaluationResult = this.EvaluateCircuit();
            if (evaluationResult.IsError) return new Result<bool>(evaluationResult.Error);

            for (int i = 0; i < this.Inputs.Count; i++)
            {
                var input = this.Inputs[i];
                var originalValue = originalInputs[i];

                input.Value = originalValue;
            }

            return new Result<bool>(evaluationResult.Value);
        }

        private bool? EvaluateTree(TreeNode<ICircuitComponent> rootNode)
        {
            if (rootNode == null) return null;

            var logicComponent = rootNode.Item;

            if (logicComponent is Input inputComp)
            {
                return inputComp.Value;
            }

            var firstInput = EvaluateTree(rootNode.Left);
            var secondInput = EvaluateTree(rootNode.Right);

            if (firstInput == null && secondInput == null)
            {
                return null;
            }

            if (firstInput != null && secondInput == null)
            {
                return logicComponent.ExecuteAction(firstInput.Value);
            }

            if (firstInput == null && secondInput != null)
            {
                return logicComponent.ExecuteAction(secondInput.Value);
            }

            return logicComponent.ExecuteAction(firstInput.Value, secondInput.Value);
        }

        private Result<bool> ValidateCircuit()
        {
            if (Inputs.Count == 0) return new Result<bool>("The circuit doesn't contain any inputs.");
            if (Output == null) return new Result<bool>("The circuit has no outputs.");

            var inputsAllConnected = Inputs.All(comp => comp.ConnectedComponentsOut.Count >= 1);
            var outputConnected = Output.ConnectedComponentsIn.Count == 1;
            var componentsAllConnected = InnerComponents.All(comp => comp.ConnectedComponentsIn.Count == comp.MaxInputs
                                                                  && comp.ConnectedComponentsOut.Count == 1);

            if (!inputsAllConnected || !outputConnected || !componentsAllConnected)
            {
                return new Result<bool>("The circuit is not fully connected");
            }

            return new Result<bool>(true);
        }

        public Result<TreeNode<ICircuitComponent>> ConstructBinaryExpressionTree()
        {
            var validationResult = ValidateCircuit();
            if (validationResult.IsError) return new Result<TreeNode<ICircuitComponent>>(validationResult.Error);

            var rootComponent = this.Output.ConnectedComponentsIn.ElementAtOrDefault(0);
            if (rootComponent == null) return new Result<TreeNode<ICircuitComponent>>("Output has no components connected to it.");

            var rootNode = ConstructTree(rootComponent);

            return new Result<TreeNode<ICircuitComponent>>(rootNode);
        }

        private TreeNode<ICircuitComponent> ConstructTree(ICircuitComponent rootComponent)
        {
            if (rootComponent == null) return null;

            var left = rootComponent.ConnectedComponentsIn.ElementAtOrDefault(0);
            var right = rootComponent.ConnectedComponentsIn.ElementAtOrDefault(1);

            var node = new TreeNode<ICircuitComponent>(rootComponent)
            {
                Left = ConstructTree(left),
                Right = ConstructTree(right)
            };

            return node;
        }

        public Result<string> ConstructBooleanAlgebraInfixExpression()
        {
            var rootNodeResult = ConstructBinaryExpressionTree();
            if (rootNodeResult.IsError) return new Result<string>(rootNodeResult.Error);

            var rootNode = rootNodeResult.Value;

            var expression = ConstructInfixExpression(rootNode, null);

            return new Result<string>(value: expression);
        }

        private string ConstructInfixExpression(TreeNode<ICircuitComponent> rootNode, ICircuitComponent previousComponent)
        {
            if (rootNode == null) return string.Empty;

            var stringBuilder = new StringBuilder();

            var logicComponent = rootNode.Item;

            bool isOperator(ICircuitComponent component) => !(component is Input || component is Output);

            var lowerPrecedenceThanPrevious = isOperator(logicComponent) &&
                                              previousComponent != null && isOperator(previousComponent) &&
                                              previousComponent.Precedence() < logicComponent.Precedence();

            if (isOperator(logicComponent) && lowerPrecedenceThanPrevious)
            {
                stringBuilder.Append("(");
            }

            if (rootNode.Left == null)
            {
                stringBuilder.Append(logicComponent.ComponentView.AlgebraLabel);
                stringBuilder.Append(ConstructInfixExpression(rootNode.Right, logicComponent));
            }
            else if (rootNode.Right == null)
            {
                stringBuilder.Append(logicComponent.ComponentView.AlgebraLabel);
                stringBuilder.Append(ConstructInfixExpression(rootNode.Left, logicComponent));
            }
            else
            {
                stringBuilder.Append(ConstructInfixExpression(rootNode.Left, logicComponent));
                if (!(logicComponent is AndGate)) stringBuilder.Append(logicComponent.ComponentView.AlgebraLabel); // Uses implicit AND e.g. A*B = AB
                stringBuilder.Append(ConstructInfixExpression(rootNode.Right, logicComponent));
            }

            if (isOperator(logicComponent) && lowerPrecedenceThanPrevious)
            {
                stringBuilder.Append(")");
            }

            return stringBuilder.ToString();
        }      

        public void DebugPrintTree(TreeNode<ICircuitComponent> rootNode)
        {
            if (rootNode == null) return;

            Console.WriteLine($"Node: {rootNode.Item.ComponentView.AlgebraLabel}");

            if (rootNode.Left != null)
            {
                Console.WriteLine($"{rootNode.Item.ComponentView.AlgebraLabel} Left:");
                DebugPrintTree(rootNode.Left);
            }

            if (rootNode.Right != null)
            {
                Console.WriteLine($"{rootNode.Item.ComponentView.AlgebraLabel} Right:");
                DebugPrintTree(rootNode.Right);
            }
        }
    }
}
