﻿using BooleanLogicSim.Circuit.Logic.Components;
using BooleanLogicSim.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSim.Model.BooleanAlgebra.Optimisation
{
    public static class SimplifyTerms
    {
        public static IEnumerable<ProductTerm> BooleanSimplify(IEnumerable<ProductTerm> minterms)
        {
            var primeImplicants = GetPrimeImplicants(minterms);

            var mintermValues = minterms.Select(term => term.DecimalValue);

            var essentialPrimeImplicants = primeImplicants.Where(pi => minterms.Count(minterm => pi.DecimalValue == minterm.DecimalValue
                                                                                              || pi.FlatCombinedTerms.Any(term => term.DecimalValue == minterm.DecimalValue)) == 1);

            var essentialsCoverAllMinterms = essentialPrimeImplicants.Count() == primeImplicants.Count();
            if (essentialsCoverAllMinterms) return essentialPrimeImplicants;

            var minimumExpression = FindMinimumCoveringExpression(minterms, primeImplicants);

            return minimumExpression;
        }

        // Quine-Mccluskey Algorithm

        private static IEnumerable<ProductTerm> GetPrimeImplicants(IEnumerable<ProductTerm> terms)
        {
            IEnumerable<ProductTerm> currentTerms = terms;

            var combined = CombineTerms(currentTerms);

            while (combined.Count() > 0)
            {
                currentTerms = currentTerms.Except(combined.SelectMany(term => term.CombinedTerms))
                                           .Concat(combined)
                                           .Distinct();

                combined = CombineTerms(currentTerms);
            }

            return currentTerms;
        }

        private static IEnumerable<ProductTerm> CombineTerms(IEnumerable<ProductTerm> terms)
        {
            var groupedByOneBits = terms.GroupBy(term => term.Values.Count(pair => pair.Value == true))
                                        .OrderBy(group => group.Key);

            var combined = new List<ProductTerm>();

            for (var i = 0; i < groupedByOneBits.Count() - 1; i++)
            {
                var group1 = groupedByOneBits.ElementAt(i);

                var group2 = groupedByOneBits.ElementAt(i + 1);

                foreach (var term1 in group1)
                {
                    foreach (var term2 in group2)
                    {
                        var diff = term1.Values
                                        .Zip(term2.Values, (first, second) => new Tuple<bool?, bool?>(first.Value, second.Value))
                                        .Count(pair => pair.Item1 != pair.Item2);

                        if (diff == 1)
                        {
                            var combinedTerm = ProductTerm.Combine(term1, term2);
                            combined.Add(combinedTerm);
                        }
                    }
                }
            }

            return combined.Distinct();
        }

        // Petrick's Method

        private static IEnumerable<ProductTerm> FindMinimumCoveringExpression(IEnumerable<ProductTerm> minterms, IEnumerable<ProductTerm> primeImplicants)
        {
            var mintermValues = minterms.Select(term => term.DecimalValue);

            var sums = new List<PetrickSum>();

            foreach (var mintermValue in mintermValues)
            {
                var coveringPrimeImplicants = primeImplicants.Where(pi => pi.DecimalValue == mintermValue || pi.FlatCombinedTerms.Any(term => term.DecimalValue == mintermValue));

                var coveringProducts = coveringPrimeImplicants.Select(imp => PetrickProduct.Single(imp)).ToList();

                sums.Add(new PetrickSum(coveringProducts));
            }

            var multipliedOutSums = MultiplyOutSums(sums);

            var minimum = multipliedOutSums.SelectMany(sum => sum.Terms)
                                           .OrderBy(prod => prod.Terms.Count)
                                           .First();

            return minimum.Terms;
        }

        private static List<PetrickSum> MultiplyOutSums(List<PetrickSum> sums)
        {
            while (sums.Count >= 2)
            {
                var sum1 = sums.ElementAt(0);
                var sum2 = sums.ElementAt(1);

                var multipliedOutTerms = new List<PetrickProduct>();

                foreach (var term1 in sum1.Terms)
                {
                    foreach (var term2 in sum2.Terms)
                    {

                        if (term1.Equals(term2))
                        {
                            // X * X = X
                            multipliedOutTerms.Add(term1);
                        }
                        else
                        {
                            // (... + X + ...)(... + Y + ...) = (... + XY + ...)
                            multipliedOutTerms.Add(new PetrickProduct(term1.Terms.Concat(term2.Terms).Distinct().ToList()));
                        }
                    }
                }

                var simplified = SimplifySum(multipliedOutTerms);

                sums.Remove(sum1);
                sums.Remove(sum2);

                sums.Insert(0, new PetrickSum(simplified));
            }

            return sums;
        }

        private static List<PetrickProduct> SimplifySum(List<PetrickProduct> sum)
        {
            var currentTerms = sum.ToList();

            var previousCount = currentTerms.Count;

            var toRemove = new List<PetrickProduct>();

            do
            {
                toRemove.Clear();

                foreach (var prod1 in currentTerms)
                {
                    foreach (var prod2 in currentTerms)
                    {
                        // X + XY = X
                        if (!prod2.Equals(prod1) && prod1.Terms.All(prod2.Terms.Contains))
                        {
                            toRemove.Add(prod2);
                        }
                    }
                }

                toRemove.Distinct().ToList().ForEach(prod => currentTerms.Remove(prod));
            } while (toRemove.Count > 0);

            // X + X = X
            var simplified = currentTerms.Distinct();

            return simplified.ToList();
        }

        // Custom Component Simplification

        public static Result<List<ISimplificationExpressionTerm>> SimplifyCustomComponents(IEnumerable<ISimplificationExpressionTerm> terms, IComponentRepository componentRepository, List<string> inputSymbols)
        {
            var customTermResults = componentRepository.RetrieveComponents().OfType<CustomComponent>()
                                                       .Select(comp => Tuple.Create(comp, BooleanExpression.ConvertToMinterms(comp.LogicCircuit).FMap(BooleanSimplify)));

            var errorTerm = customTermResults.FirstOrDefault(tup => tup.Item2.IsError);
            if (errorTerm != null) return new Result<List<ISimplificationExpressionTerm>>(errorTerm.Item2.Error);

            var customTerms = customTermResults.Select(tup => Tuple.Create(tup.Item1, tup.Item2.Value))
                                               .OrderByDescending(tup => tup.Item2.Count());

            var currentTerms = new List<ISimplificationExpressionTerm>(terms);

            var appliedSimplification = false;

            do
            {
                appliedSimplification = false;

                foreach (var tuple in customTerms)
                {
                    var comp = tuple.Item1;
                    var compTerms = tuple.Item2;

                    var termsToRemove = new List<ProductTerm>();
                    var inputsUsed = new List<string>();

                    var foundSimplification = false;

                    foreach (var input1 in inputSymbols)
                    {
                        if (foundSimplification) break;

                        foreach (var input2 in inputSymbols)
                        {
                            if (foundSimplification) break;
                            if (input1 == input2) continue;

                            string[] inputs = { input1, input2 };
                            var inputMatchedCompTerms = ProductTerm.ChangeVariables(compTerms, inputs);

                            var toRemove = inputMatchedCompTerms.Select(compTerm => currentTerms.OfType<ProductTerm>()
                                                                                                .FirstOrDefault(term2 => term2.Contains(compTerm)))
                                                                .Where(term => term != null);

                            if (toRemove.Count() == compTerms.Count())
                            {
                                termsToRemove.AddRange(toRemove);
                                inputsUsed.AddRange(inputs);

                                foundSimplification = true;
                            }
                        }
                    }

                    if (termsToRemove.Count() != 0)
                    {
                        termsToRemove.ForEach(term => currentTerms.Remove(term));

                        currentTerms.Add(new CustomTerm(comp, inputsUsed));

                        appliedSimplification = true;
                    }
                }

            } while (appliedSimplification);

            return new Result<List<ISimplificationExpressionTerm>>(currentTerms);
        }
    }
}
