﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSim.Model.BooleanAlgebra
{
    public class PairComparer<T, U> : IEqualityComparer<KeyValuePair<T, U>>
    {
        public bool Equals(KeyValuePair<T, U> x, KeyValuePair<T, U> y)
        {
            return x.Key.Equals(y.Key) && x.Value.Equals(y.Value);
        }

        public int GetHashCode(KeyValuePair<T, U> obj)
        {
            return 5 + obj.Key.GetHashCode() + obj.Value.GetHashCode();
        }
    }
}
