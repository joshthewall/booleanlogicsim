﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSim.Model.BooleanAlgebra.Optimisation
{
    public interface ISimplificationExpressionTerm
    {
        string AsInfixExpression();
    }

    public static class SimplificationExpressionTerms
    {
        public static string ConstructInfixExpression(IEnumerable<ISimplificationExpressionTerm> terms)
        {
            var expressionBuilder = new StringBuilder();

            foreach (var term in terms)
            {
                expressionBuilder.Append($"{term.AsInfixExpression()} + ");
            }

            var expression = expressionBuilder.ToString().TrimEnd(new char[] { '+', ' ' });

            return expression;
        }
    }
}
