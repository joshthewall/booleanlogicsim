﻿using BooleanLogicSim.Model.BooleanAlgebra.Optimisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSim.Model.BooleanAlgebra
{
    public class ProductTerm : IEquatable<ProductTerm>, ISimplificationExpressionTerm
    {
        public int? DecimalValue { get; }

        private readonly List<KeyValuePair<string, bool?>> _values = new List<KeyValuePair<string, bool?>>();

        public List<KeyValuePair<string, bool?>> Values
        {
            get
            {
                return _values.OrderBy(val => val.Key).ToList();
            }
        }

        public List<ProductTerm> CombinedTerms { get; } = new List<ProductTerm>();

        private string _binaryString = null;

        public string BinaryString
        {
            get
            {
                if (_binaryString != null) return _binaryString;

                _binaryString = String.Concat(this.Values.Select(pair =>
                {
                    var value = pair.Value;

                    if (value != null)
                    {
                        return ((bool)value) ? '1' : '0';
                    }
                    else
                    {
                        return '-';
                    }
                }));

                return _binaryString;
            }
        }

        private List<ProductTerm> _flatCombinedTerms = null;

        public List<ProductTerm> FlatCombinedTerms
        {
            get
            {
                if (_flatCombinedTerms != null) return _flatCombinedTerms;

                IEnumerable<ProductTerm> combined = new List<ProductTerm>(this.CombinedTerms);

                while (combined.Any(term => term.CombinedTerms.Count() > 0))
                {
                    combined = combined.SelectMany(term => term.CombinedTerms);
                }

                _flatCombinedTerms = combined.ToList();

                return _flatCombinedTerms;
            }
        }

        public ProductTerm(List<KeyValuePair<string, bool?>> values, int decimalValue)
        {
            this._values = values;
            this.DecimalValue = decimalValue;
        }

        public ProductTerm(List<KeyValuePair<string, bool>> values, int decimalValue)
        {
            this._values = values.Select(pair => new KeyValuePair<string, bool?>(pair.Key, pair.Value)).ToList();
            this.DecimalValue = decimalValue;
        }

        public ProductTerm(List<KeyValuePair<string, bool?>> values, List<ProductTerm> combinedTerms)
        {
            this._values = values;
            this.CombinedTerms = combinedTerms;
            this.DecimalValue = null;
        }

        public static ProductTerm Combine(ProductTerm term1, ProductTerm term2)
        {
            var combinedValues = term1.Values.Zip(term2.Values, (first, second) =>
            {
                if (first.Value == second.Value)
                {
                    return first;
                }
                else
                {
                    return new KeyValuePair<string, bool?>(first.Key, null);
                }
            });

            var combined = new ProductTerm(combinedValues.ToList(), new List<ProductTerm>() { term1, term2 });

            return combined;
        }

        public static ProductTerm ChangeVariables(ProductTerm term, IEnumerable<string> variables)
        {
            var variableChangedValues = term.Values.Zip(variables, (val, input) => new KeyValuePair<string, bool?>(input, val.Value)).ToList();

            if (term.CombinedTerms.Count > 0)
            {
                return new ProductTerm(variableChangedValues, term.CombinedTerms);
            }
            else
            {
                return new ProductTerm(variableChangedValues, term.DecimalValue.Value);
            }
        }

        public static IEnumerable<ProductTerm> ChangeVariables(IEnumerable<ProductTerm> terms, IEnumerable<string> variables)
            => terms.Select(term => ChangeVariables(term, variables));

        public string AsInfixExpression()
        {
            var exprBuilder = new StringBuilder();

            foreach (var value in this.Values)
            {
                if (value.Value == null) continue;

                var notValue = (bool)!(value.Value);

                if (notValue)
                {
                    exprBuilder.Append("~");
                }

                exprBuilder.Append(value.Key);
            }

            return exprBuilder.ToString();
        }

        public static PairComparer<string, bool?> PairComparer { get; } = new PairComparer<string, bool?>();

        public bool Contains(ProductTerm other)
            => other.Values.All(otherVal => this.Values.Contains(otherVal, PairComparer))
            && this.Values.Except(other.Values.Where(otherVal => this.Values.Contains(otherVal, PairComparer))).All(val => val.Value == null);

        public bool Equals(ProductTerm other) => this.Equals(other as object);

        public override bool Equals(object obj) =>
            obj is ProductTerm term
            && this.Values.Zip(term.Values, (first, second) => PairComparer.Equals(first, second)).All(val => val == true);

        public override int GetHashCode()
        {
            return 1291433875 + this.Values.Aggregate(0, (acc, pair) => acc + PairComparer.GetHashCode(pair));
        }
    }
}
