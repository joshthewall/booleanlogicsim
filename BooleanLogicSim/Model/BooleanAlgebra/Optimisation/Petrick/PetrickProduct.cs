﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSim.Model.BooleanAlgebra.Optimisation
{
    public class PetrickProduct : IEquatable<PetrickProduct>
    {
        public List<ProductTerm> Terms { get; }

        public PetrickProduct(List<ProductTerm> terms)
        {
            this.Terms = terms;
        }

        public static PetrickProduct Single(ProductTerm term) => new PetrickProduct(new List<ProductTerm>() { term });

        public override bool Equals(object obj)
        {
            return obj is PetrickProduct product && this.Terms.All(product.Terms.Contains) && this.Terms.Count == product.Terms.Count;
        }

        public override int GetHashCode()
        {
            return -1073114708 + this.Terms.Aggregate(0, (acc, term) => acc + term.GetHashCode());
        }

        public bool Equals(PetrickProduct other)
        {
            var product = other as PetrickProduct;
            return product != null && this.Terms.All(product.Terms.Contains) && this.Terms.Count == product.Terms.Count;
        }
    }
}
