﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSim.Model.BooleanAlgebra.Optimisation
{
    public class PetrickSum
    {
        public List<PetrickProduct> Terms { get; }

        public PetrickSum(List<PetrickProduct> terms)
        {
            this.Terms = terms;
        }

        public override bool Equals(object obj)
        {
            var sum = obj as PetrickSum;
            return sum != null && this.Terms.All(sum.Terms.Contains) && this.Terms.Count == sum.Terms.Count;
        }

        public override int GetHashCode()
        {
            return -1073114708 + this.Terms.Aggregate(0, (acc, term) => acc + term.GetHashCode());
        }
    }
}
