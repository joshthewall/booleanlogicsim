﻿using BooleanLogicSim.Circuit.Logic.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSim.Model.BooleanAlgebra.Optimisation
{
    public class CustomTerm : ISimplificationExpressionTerm
    {
        public CustomComponent Component { get; }
        public List<string> Inputs { get; }

        public CustomTerm(CustomComponent component, List<string> inputs)
        {
            this.Component = component;
            this.Inputs = inputs;
        }

        public string AsInfixExpression() => Inputs.ElementAt(0) + Component.ComponentView.AlgebraLabel + Inputs.ElementAt(1);
    }
}
