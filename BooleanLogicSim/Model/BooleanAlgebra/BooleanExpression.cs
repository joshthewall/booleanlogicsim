﻿using BooleanLogicSim.Circuit;
using BooleanLogicSim.Model.LogicCircuits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooleanLogicSim.Data;
using BooleanLogicSim.Model.BooleanAlgebra.Optimisation;
using BooleanLogicSim.Circuit.Logic.Components;
using BooleanLogicSim.Circuit.Logic;

namespace BooleanLogicSim.Model.BooleanAlgebra
{
    public class BooleanExpression
    {
        public string InfixExpression { get; }

        public List<string> InputSymbols => EquivalentLogicCircuit.Inputs.Select(input => input.ComponentView.AlgebraLabel).ToList();

        public LogicCircuit EquivalentLogicCircuit { get; private set; }

        private IComponentRepository ComponentRepository { get; set; }

        private BooleanTokenizer BooleanTokenizer { get; } = new BooleanTokenizer();

        public static Result<BooleanExpression> FromInfixString(string infixExpression, IComponentRepository componentRepository)
        {
            if (string.IsNullOrWhiteSpace(infixExpression)) return new Result<BooleanExpression>("There is no expression to convert.");

            var noWhiteSpaceExpression = new string(infixExpression
                                                       .Where(c => !char.IsWhiteSpace(c))
                                                       .ToArray());

            var expression = new BooleanExpression(noWhiteSpaceExpression, componentRepository);

            var circuitResult = expression.ConvertToCircuit();
            if (circuitResult.IsError) return new Result<BooleanExpression>(circuitResult.Error);

            expression.EquivalentLogicCircuit = circuitResult.Value;

            return new Result<BooleanExpression>(expression);
        }

        private BooleanExpression(string infixExpression, IComponentRepository componentRepository)
        {
            this.InfixExpression = infixExpression;
            this.ComponentRepository = componentRepository;
        }

        public Result<BooleanExpression> SimplifyExpression(bool simplifyCustomComponents)
        {
            var mintermsResult = ConvertToMinterms(this.EquivalentLogicCircuit);
            if (mintermsResult.IsError) return new Result<BooleanExpression>(mintermsResult.Error);

            var minterms = mintermsResult.Value;

            var noInputsYieldTrue = minterms.Count == 0;
            if (noInputsYieldTrue) return new Result<BooleanExpression>("This expression is a contradiction.");

            var maxNumberOfTerms = (int)Math.Pow(2, this.EquivalentLogicCircuit.Inputs.Count);

            var allInputsYieldTrue = minterms.Count == maxNumberOfTerms;
            if (allInputsYieldTrue) return new Result<BooleanExpression>("This expression is a tautology.");

            var simplifiedTerms = SimplifyTerms.BooleanSimplify(minterms).Cast<ISimplificationExpressionTerm>();

            if (simplifyCustomComponents)
            {
                var customSimplifiedResult = SimplifyTerms.SimplifyCustomComponents(simplifiedTerms, this.ComponentRepository, this.InputSymbols);
                if (customSimplifiedResult.IsError) return new Result<BooleanExpression>(customSimplifiedResult.Error);

                simplifiedTerms = customSimplifiedResult.Value;
            }

            var simplifiedSOPExpression = SimplificationExpressionTerms.ConstructInfixExpression(simplifiedTerms);

            bool isComplexCharacter(char chr) => chr != '(' && chr != ')' && !char.IsWhiteSpace(chr);

            if (simplifiedSOPExpression.Count(isComplexCharacter) > this.InfixExpression.Count(isComplexCharacter))
                return new Result<BooleanExpression>(this);

            var expressionResult = FromInfixString(simplifiedSOPExpression, this.ComponentRepository);
            if (expressionResult.IsError) return new Result<BooleanExpression>(expressionResult.Error);

            var expression = expressionResult.Value;

            return new Result<BooleanExpression>(expression);
        }

        public static Result<List<ProductTerm>> ConvertToMinterms(LogicCircuit logicCircuit)
        {
            var terms = new List<ProductTerm>();

            var inputs = logicCircuit.Inputs;

            var maxNumberOfTerms = (int)Math.Pow(2, inputs.Count);

            var evaluationInputs = new List<KeyValuePair<string, bool>>();

            var count = (int)Math.Pow(2, inputs.Count);

            for (var i = 0; i < count; i++)
            {
                var binaryString = Convert.ToString(i, 2).PadLeft(inputs.Count, '0');

                for (var j = 0; j < binaryString.Length; j++)
                {
                    var input = inputs[j];
                    var bit = binaryString[j];

                    if (bit == '1')
                    {
                        evaluationInputs.Add(new KeyValuePair<string, bool>(input.ComponentView.AlgebraLabel, true));
                    }
                    else
                    {
                        evaluationInputs.Add(new KeyValuePair<string, bool>(input.ComponentView.AlgebraLabel, false));
                    }
                }

                var evaluationResult = logicCircuit.EvaluateCircuit(evaluationInputs.ToDictionary(pair => pair.Key, pair => pair.Value));
                if (evaluationResult.IsError)
                {
                    return new Result<List<ProductTerm>>($"There was an error evaluating the expression:\n{evaluationResult.Error}");
                }

                var isMinterm = evaluationResult.Value == true;

                if (isMinterm)
                {
                    terms.Add(new ProductTerm(evaluationInputs.ToList(), i));
                }

                evaluationInputs.Clear();
            }

            return new Result<List<ProductTerm>>(terms);
        }

        public Result<bool> EvaluateExpression(Dictionary<string, bool> inputs) => this.EquivalentLogicCircuit.EvaluateCircuit(inputs);

        public Result<TreeNode<string>> ConvertToTree()
        {
            var outputQueueResult = this.ConvertToPostfixOutputQueue();
            if (outputQueueResult.IsError) return new Result<TreeNode<string>>(outputQueueResult.Error);

            var outputQueue = outputQueueResult.Value;

            var treeNodeStack = new Stack<TreeNode<string>>();

            while (outputQueue.Count > 0)
            {
                var token = outputQueue.Dequeue();

                if (token.TokenType == TokenType.Value)
                {
                    treeNodeStack.Push(new TreeNode<string>(token.Value));
                }
                else if (token.TokenType == TokenType.BinaryOperator)
                {
                    if (treeNodeStack.Count < 2)
                    {
                        return new Result<TreeNode<string>>("An operator was missing at least one of its operands.");
                    }

                    var secondInput = treeNodeStack.Pop();
                    var firstInput = treeNodeStack.Pop();

                    var operatorNode = new TreeNode<string>(token.Value)
                    {
                        Left = firstInput,
                        Right = secondInput
                    };

                    treeNodeStack.Push(operatorNode);
                }
                else if (token.TokenType == TokenType.UnaryOperator)
                {
                    if (treeNodeStack.Count < 1)
                    {
                        return new Result<TreeNode<string>>("An operator was missing its operand.");
                    }

                    var input = treeNodeStack.Pop();

                    var operatorNode = new TreeNode<string>(token.Value)
                    {
                        Left = input
                    };

                    treeNodeStack.Push(operatorNode);
                }
                else
                {
                    return new Result<TreeNode<string>>("Invalid token type in tree conversion input");
                }
            }

            if (treeNodeStack.Count == 0)
            {
                return new Result<TreeNode<string>>("No values were found in the expression.");
            }

            return new Result<TreeNode<string>>(treeNodeStack.Pop());
        }

        public Result<string> ConvertToPostfix()
        {
            var outputQueueResult = this.ConvertToPostfixOutputQueue();
            if (outputQueueResult.IsError) return new Result<string>(error: outputQueueResult.Error);

            var outputQueue = outputQueueResult.Value;

            return new Result<string>(value: String.Join(" ", outputQueue.Select(token => token.Value)));
        }

        private Result<Queue<Token>> ConvertToPostfixOutputQueue()
        {
            var tokens = BooleanTokenizer.Tokenize(this.InfixExpression);
            var tokenStack = new Stack<Token>(tokens.Reverse<Token>());

            var operatorStack = new Stack<Token>();
            var outputQueue = new Queue<Token>();

            while (tokenStack.Count > 0)
            {
                var token = tokenStack.Pop();

                if (token.TokenType == TokenType.Value)
                {
                    if (tokenStack.Count > 0 && TokenTypes.FollowImplicitAND.Contains(tokenStack.Peek().TokenType))
                    {
                        tokenStack.Push(new Token("*", TokenType.BinaryOperator));
                    }

                    outputQueue.Enqueue(token);
                    continue;
                }

                if (TokenTypes.Operators.Contains(token.TokenType))
                {
                    var tokenPrecedence = TokenTypes.OperatorPrecedences[token.Value];

                    while (operatorStack.Count > 0
                        && operatorStack.Peek().TokenType != TokenType.LeftBracket
                        && (TokenTypes.OperatorPrecedences[operatorStack.Peek().Value] <= tokenPrecedence))
                    {
                        outputQueue.Enqueue(operatorStack.Pop());
                    }

                    operatorStack.Push(token);

                    continue;
                }

                if (token.TokenType == TokenType.LeftBracket)
                {
                    operatorStack.Push(token);

                    continue;
                }

                if (token.TokenType == TokenType.RightBracket)
                {
                    while (operatorStack.Count > 0 && operatorStack.Peek().TokenType != TokenType.LeftBracket)
                    {
                        outputQueue.Enqueue(operatorStack.Pop());
                    }

                    if (operatorStack.Count == 0)
                    {
                        return new Result<Queue<Token>>("Missing left bracket in Boolean algebra expression.");
                    }

                    if (tokenStack.Count > 0 && TokenTypes.FollowImplicitAND.Contains(tokenStack.Peek().TokenType))
                    {
                        tokenStack.Push(new Token("*", TokenType.BinaryOperator));
                    }

                    operatorStack.Pop();

                    continue;
                }

                return new Result<Queue<Token>>("Tried to parse expression with unknown token type");
            }

            if (tokenStack.Count == 0)
            {
                while (operatorStack.Count > 0)
                {
                    var op = operatorStack.Pop();

                    if (TokenTypes.Brackets.Contains(op.TokenType))
                    {
                        return new Result<Queue<Token>>("Unbalanced brackets in Boolean algebra expression.");
                    }

                    outputQueue.Enqueue(op);
                }
            }

            return new Result<Queue<Token>>(outputQueue);
        }

        private Result<LogicCircuit> ConvertToCircuit()
        {
            var rootNodeResult = this.ConvertToTree();
            if (rootNodeResult.IsError) return new Result<LogicCircuit>(rootNodeResult.Error);

            var rootNode = rootNodeResult.Value;

            var rootComponent = ConvertToCircuitComponents(rootNode, new List<Input>());

            var rootOutputComponent = new Output();

            rootOutputComponent.Connect(rootComponent);

            var circuit = new LogicCircuit();

            ConstructCircuit(rootOutputComponent, circuit);

            return new Result<LogicCircuit>(circuit);
        }

        private ICircuitComponent ConvertToCircuitComponents(TreeNode<string> rootNode, List<Input> inputs)
        {
            if (rootNode == null) return null;

            var component = ComponentRepository.GetComponentBySymbol(rootNode.Item) as ICircuitComponent;
            if (component == null) return null;

            if (component is Input inputComponent)
            {
                var savedInput = inputs.FirstOrDefault(input => input.ComponentView.AlgebraLabel == component.ComponentView.AlgebraLabel);

                if (savedInput != null)
                {
                    component = savedInput;
                }
                else
                {
                    inputs.Add(inputComponent);
                }
            }

            var left = ConvertToCircuitComponents(rootNode.Left, inputs);
            var right = ConvertToCircuitComponents(rootNode.Right, inputs);

            if (left != null) component.Connect(left);
            if (right != null) component.Connect(right);

            return component;
        }

        private static void ConstructCircuit(ICircuitComponent rootComponent, LogicCircuit circuit)
        {
            if (rootComponent == null) return;

            circuit.AddComponent(rootComponent);

            rootComponent.ConnectedComponentsIn.ForEach(comp => ConstructCircuit(comp, circuit));
        }
    }
}
