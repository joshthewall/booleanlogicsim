﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSim.Model.BooleanAlgebra
{
    public enum TokenType
    {
        BinaryOperator,
        UnaryOperator,
        Value,
        LeftBracket,
        RightBracket
    }

    public class TokenTypes
    {
        public static List<TokenType> Operators { get; } = new List<TokenType>() { TokenType.BinaryOperator, TokenType.UnaryOperator };

        public static List<TokenType> Brackets { get; } = new List<TokenType>() { TokenType.LeftBracket, TokenType.RightBracket };

        public static List<TokenType> FollowImplicitAND = new List<TokenType>() { TokenType.Value, TokenType.LeftBracket, TokenType.UnaryOperator };

        public static Dictionary<string, TokenType> Tokens { get; } = new Dictionary<string, TokenType>() {
            { "+", TokenType.BinaryOperator },
            { "*", TokenType.BinaryOperator },
            { "~", TokenType.UnaryOperator },
            { "(", TokenType.LeftBracket },
            { ")", TokenType.RightBracket }
        };

        public static Dictionary<string, int> OperatorPrecedences { get; } = new Dictionary<string, int>()
        {
            { "+", 3 },
            // 2 is for custom operators
            { "*", 1 },
            { "~", 0},
        };
    }
}
