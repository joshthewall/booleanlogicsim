﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSim.Model.BooleanAlgebra
{
    public class BooleanTokenizer
    {
        public List<Token> Tokenize(string input)
        {
            var tokens = new List<Token>();

            foreach (var chr in input)
            {
                string tokenValue = chr.ToString();

                if (!TokenTypes.Tokens.TryGetValue(tokenValue, out var tokenType))
                {
                    tokenType = TokenType.Value;
                }

                tokens.Add(new Token(tokenValue, tokenType));
            }

            return tokens;
        }
    }
}
