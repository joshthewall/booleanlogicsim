﻿namespace BooleanLogicSim.Model.BooleanAlgebra
{
    public class Token
    {
        public string Value { get; set; }
        public TokenType TokenType { get; set; }

        public Token(string value, TokenType tokenType)
        {
            this.Value = value;
            this.TokenType = tokenType;
        }
    }
}