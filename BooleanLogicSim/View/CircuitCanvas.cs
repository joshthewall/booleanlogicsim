﻿using BooleanLogicSim.CanvasElements;
using BooleanLogicSim.Circuit;
using BooleanLogicSim.Circuit.Logic;
using BooleanLogicSim.Circuit.Logic.Components;
using BooleanLogicSim.Circuit.View.Canvas;
using BooleanLogicSim.Data;
using BooleanLogicSim.Extensions;
using BooleanLogicSim.Model;
using BooleanLogicSim.Model.BooleanAlgebra;
using BooleanLogicSim.Model.LogicCircuits;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using static BooleanLogicSim.Circuit.Logic.Components.CustomComponent;

namespace BooleanLogicSim.View
{
    public class CircuitCanvas
    {
        private LogicCircuit _logicCircuit;

        public LogicCircuit LogicCircuit
        {
            get => _logicCircuit;

            set
            {
                _logicCircuit = value;

                this.Clear();
                this.AddComponentsFromLogicCircuit();
            }
        }

        private Canvas MainCanvas { get; }

        private MainWindow MainWindow { get; }

        private PanZoomTransformGroup PanZoomTransform { get; } = new PanZoomTransformGroup();

        private IComponentRepository ComponentRepository { get; }

        public CircuitCanvas(MainWindow mainWindow, Canvas mainCanvas, IComponentRepository componentRepository, LogicCircuit logicCircuit)
        {
            this.MainWindow = mainWindow;
            this.MainCanvas = mainCanvas;
            this.ComponentRepository = componentRepository;
            this.LogicCircuit = logicCircuit;

            MainCanvas.RenderTransform = PanZoomTransform.TransformGroup;

            MainCanvas.DragOver += (s, args) => OnCanvasDragOver(s, args);
            MainCanvas.Drop += (s, args) => OnCanvasDrop(s, args);

            MainCanvas.MouseWheel += (s, args) => OnCanvasMouseWheel(s, args);
            MainCanvas.MouseLeftButtonDown += (s, args) => OnCanvasMouseLeftButtonDown(s, args);
            MainCanvas.MouseLeftButtonUp += (s, args) => OnCanvasMouseLeftButtonUp(s, args);
            MainCanvas.MouseMove += (s, args) => OnCanvasMouseMove(s, args);
        }

        private void OnCanvasDrop(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(MainWindow.ComponentListBoxDataIdentifier))
            {
                e.Effects = DragDropEffects.None;
                return;
            }

            var componentName = (string)e.Data.GetData(MainWindow.ComponentListBoxDataIdentifier);
            var component = ComponentRepository.GetComponentByName(componentName) as ICircuitComponent;

            var circuitComponent = new CircuitCanvasComponent();
            circuitComponent.SetComponent(component);
            circuitComponent.SetLogicCircuit(LogicCircuit);
            circuitComponent.SetMainWindow(MainWindow);

            var pos = e.GetPosition(MainCanvas);

            AddCanvasElementOnCenter(circuitComponent, pos);

            component.ComponentView.Position = pos;
        }

        private void OnCanvasMouseWheel(object sender, MouseWheelEventArgs e)
        {
            var position = e.GetPosition(MainCanvas);

            var zoomType = e.Delta < 0 ? ZoomType.ZoomIn : ZoomType.ZoomOut;

            PanZoomTransform.ZoomAt(position, zoomType);
        }

        private void OnCanvasMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!e.OriginalSource.Equals(MainCanvas)) return;

            MainCanvas.CaptureMouse();
        }

        private void OnCanvasMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!e.OriginalSource.Equals(MainCanvas)) return;

            MainCanvas.ReleaseMouseCapture();
        }

        private void OnCanvasMouseMove(object sender, MouseEventArgs e)
        {
            var mousePos = e.GetPosition(MainCanvas);

            if (MainCanvas.IsMouseCaptured)
            {
                PanZoomTransform.Pan(mousePos);
            }

            PanZoomTransform.MouseLastPosition = e.GetPosition(MainCanvas);
        }

        private void OnCanvasDragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(CircuitCanvasComponent.ConnectionDataObjectIdentifier))
            {
                var mousePos = e.GetPosition(MainCanvas);

                var component = (CircuitCanvasComponent)e.Data.GetData(CircuitCanvasComponent.ConnectionDataObjectIdentifier);

                component.SetConnectionLinePosition(component.PotentialConnectionLine, mousePos);
            }
        }

        public void AddCanvasElement(FrameworkElement element)
        {
            if (element is CircuitCanvasComponent circuitComponent)
            {
                if (circuitComponent.Component is Input input)
                {
                    var currentLabels = LogicCircuit.Inputs.Select(comp => comp.ComponentView.AlgebraLabel)
                                                           .Concat(TokenTypes.Tokens.Keys);

                    string algebraLabel = input.ComponentView.AlgebraLabel;

                    while (string.IsNullOrWhiteSpace(algebraLabel) && !currentLabels.Contains(algebraLabel))
                    {
                        var labelInput = Interaction.InputBox("Enter a label name for the input.", "Input Label", "");

                        if (labelInput.Length == 0) return;

                        if (currentLabels.Contains(labelInput))
                        {
                            MessageBox.Show("A component with this label already exists.", "Conflicting Input Label", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                            continue;
                        }

                        if (labelInput.Length > 1)
                        {
                            MessageBox.Show("The label must be a single symbol.", "Invalid Label", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                            continue;
                        }

                        algebraLabel = labelInput;
                    }

                    input.ComponentView.AlgebraLabel = algebraLabel;
                }

                var successfullyAdded = LogicCircuit.AddComponent(circuitComponent.Component);

                if (!successfullyAdded) return;
            }

            var eventHandler = new DraggableCanvasItemEventHandler(element, MainCanvas);
            MainCanvas.Children.Add(element);
        }

        public void AddCanvasElementOnCenter(FrameworkElement element, Point point)
        {
            SetElementPositionOnCenter(element, point);
            AddCanvasElement(element);
        }

        public void RemoveElement(FrameworkElement element)
        {
            MainCanvas.Children.Remove(element);
        }

        private void AddComponentsFromLogicCircuit()
        {
            var components = LogicCircuit.Inputs
                                         .Concat(LogicCircuit.InnerComponents)
                                         .Concat(new ICircuitComponent[] { LogicCircuit.Output })
                                         .Where(comp => comp != null);

            var useAutoPlacement = components.Any(comp => comp?.ComponentView?.Position == null
                                                       || comp?.ComponentView?.Position.Value.Equals(new Point(0, 0)) == true);

            if (useAutoPlacement)
            {
                var inputs = LogicCircuit.Inputs;

                for (int i = 0; i < inputs.Count; i++)
                {
                    var inputComp = inputs[i];
                    var height = i + 1;

                    AddCircuitComponent(inputComp, 1, height);
                }

                var output = LogicCircuit.Output;

                if (output == null) return;

                var maxDepthResult = LogicCircuit.GetMaximumDepth();
                if (maxDepthResult.IsError) return;

                var maxDepth = maxDepthResult.Value;

                AddComponents(output, maxDepth + 1, 1);
            }
            else
            {
                LogicCircuit.Inputs.ForEach(AddCircuitComponentWithPosition);
                LogicCircuit.InnerComponents.ForEach(AddCircuitComponentWithPosition);

                var output = LogicCircuit.Output;

                if (output != null)
                {
                    AddCircuitComponentWithPosition(output);
                }
            }

            var canvasComponents = new List<CircuitCanvasComponent>();

            foreach (var element in MainCanvas.Children)
            {
                if (element is CircuitCanvasComponent canvasComponent)
                {
                    canvasComponents.Add(canvasComponent);
                }
            }

            var mostRightwardComponent = canvasComponents.OrderByDescending(comp => Canvas.GetLeft(comp)).FirstOrDefault() ?? null;
            var mostDownwardComponent = canvasComponents.OrderByDescending(comp => Canvas.GetTop(comp)).FirstOrDefault() ?? null;

            if (mostRightwardComponent == null || mostDownwardComponent == null) return;

            MainCanvas.Width = Canvas.GetLeft(mostRightwardComponent) + PanZoomTransformGroup.SizeIncrement;
            MainCanvas.Height = Canvas.GetTop(mostDownwardComponent) + PanZoomTransformGroup.SizeIncrement;

            ConnectComponents(canvasComponents);
        }

        private void AddComponents(ICircuitComponent rootComponent, int depthLevel, int heightLevel)
        {
            var componentsIn = rootComponent.ConnectedComponentsIn
                                            .Where(compIn => !(compIn is Input))
                                            .OrderBy(compIn => compIn.ConnectedComponentsIn.Count).Reverse();

            var nextComponentChildrenMaxLevel = 0;

            for (var i = 0; i < componentsIn.Count(); i++)
            {
                var compIn = componentsIn.ElementAt(i);

                AddComponents(compIn, depthLevel - 1, heightLevel + i + nextComponentChildrenMaxLevel);

                nextComponentChildrenMaxLevel = compIn.ConnectedComponentsIn.Where(compInIn => !(compInIn is Input)).Count() - 1;

                if (nextComponentChildrenMaxLevel < 0)
                {
                    nextComponentChildrenMaxLevel = 0;
                }
            }

            if (!(rootComponent is Input))
            {
                AddCircuitComponent(rootComponent, depthLevel, heightLevel);
            }
        }

        private void AddCircuitComponentWithPosition(ICircuitComponent component)
        {
            if (component?.ComponentView?.Position.HasValue == false) return;

            var circuitComponent = new CircuitCanvasComponent();
            circuitComponent.SetComponent(component);
            circuitComponent.SetLogicCircuit(this.LogicCircuit);
            circuitComponent.SetMainWindow(MainWindow);

            SetElementPosition(circuitComponent, component.ComponentView.Position.Value);

            var eventHandler = new DraggableCanvasItemEventHandler(circuitComponent, MainCanvas);

            MainCanvas.Children.Add(circuitComponent);

            if (component is Output)
            {
                MainWindow.DisableOutputCreation();
            }
        }

        private void AddCircuitComponent(ICircuitComponent component, int depthLevel, int heightLevel)
        {
            var circuitComponent = new CircuitCanvasComponent();
            circuitComponent.SetComponent(component);
            circuitComponent.SetLogicCircuit(this.LogicCircuit);
            circuitComponent.SetMainWindow(MainWindow);

            var position = new Point(Math.Log(circuitComponent.Width) + depthLevel * 140 + 100,
                                                                   (heightLevel + 1) * 60);

            SetElementPositionOnCenter(circuitComponent, position);

            var eventHandler = new DraggableCanvasItemEventHandler(circuitComponent, MainCanvas);

            MainCanvas.Children.Add(circuitComponent);

            if (component is Output)
            {
                MainWindow.DisableOutputCreation();
            }
        }

        private void ConnectComponents(List<CircuitCanvasComponent> canvasComponents)
        {
            foreach (var canvasComponent in canvasComponents)
            {
                var component = canvasComponent.Component;

                var orderedComponentsIn = component.ConnectedComponentsIn.OrderBy(comp => comp.ComponentView.AlgebraLabel).ToList();

                orderedComponentsIn.ForEach(compIn =>
                {
                    var canvasComponentIn = canvasComponents.FirstOrDefault(canvasComp => compIn.Equals(canvasComp.Component));
                    if (canvasComponentIn == null) return;

                    var alreadyConnected = canvasComponent.InputRegions.Any(region => canvasComponentIn.Component?.Equals(region.ConnectedComponent?.Component) == true);
                    if (alreadyConnected) return;

                    var componentTop = Canvas.GetTop(canvasComponent);
                    var componentInTop = Canvas.GetTop(canvasComponentIn);

                    var inputRegion = canvasComponent.InputRegions
                                                     .LastOrDefault(region => !region.ConnectionPointUsed && componentInTop <= componentTop)
                                   ?? canvasComponent.InputRegions
                                                     .FirstOrDefault(region => !region.ConnectionPointUsed);

                    if (inputRegion != null)
                    {
                        canvasComponent.Connect(canvasComponentIn, inputRegion);
                        canvasComponentIn.ConnectTo(canvasComponent, inputRegion);
                    }
                });

                var orderedComponentsOut = component.ConnectedComponentsOut.OrderBy(comp => comp.ComponentView.AlgebraLabel).ToList();

                orderedComponentsOut.ForEach(compOut =>
                {
                    var canvasComponentOut = canvasComponents.FirstOrDefault(canvasComp => compOut.Equals(canvasComp.Component));
                    if (canvasComponentOut == null) return;

                    var alreadyConnected = canvasComponentOut.InputRegions.Any(region => canvasComponent.Component?.Equals(region.ConnectedComponent?.Component) == true);
                    if (alreadyConnected) return;

                    var componentTop = Canvas.GetTop(canvasComponent);
                    var componentOutTop = Canvas.GetTop(canvasComponentOut);

                    var inputRegion = canvasComponentOut.InputRegions
                                                        .LastOrDefault(region => !region.ConnectionPointUsed && componentOutTop <= componentTop)
                                   ?? canvasComponentOut.InputRegions
                                                        .FirstOrDefault(region => !region.ConnectionPointUsed);

                    if (inputRegion != null)
                    {
                        canvasComponent.ConnectTo(canvasComponentOut, inputRegion);
                        canvasComponentOut.Connect(canvasComponent, inputRegion);
                    }
                });
            }
        }

        public void UpdateCustomComponentImages(CustomComponent componentToUpdate)
        {
            foreach (var comp in MainCanvas.Children)
            {
                if (comp is CircuitCanvasComponent canvasComp && canvasComp.Component.ComponentView.PlainName == componentToUpdate.ComponentView.PlainName)
                {
                    var updatedCustomComp = this.ComponentRepository.GetComponentByName(componentToUpdate.ComponentView.PlainName) as CustomComponent;

                    var newImage = (updatedCustomComp.ComponentView as CustomComponentView).GetElement() as Image;

                    var mainElement = canvasComp.MainCanvas.Children.OfType<Image>().FirstOrDefault() as UIElement
                                   ?? canvasComp.MainCanvas.Children.OfType<Label>().FirstOrDefault();

                    if (mainElement is Image imageElement)
                    {
                        imageElement.Source = newImage.Source;
                    }
                    else if (mainElement is Label labelElement)
                    {
                        canvasComp.MainCanvas.Children.Remove(labelElement);
                        canvasComp.MainCanvas.Children.Add(newImage);
                    }
                    else
                    {
                        continue;
                    }
                }
            }
        }

        public void ResetView()
        {
            PanZoomTransform.Reset();
        }

        public void Clear()
        {
            foreach (var element in MainCanvas.Children)
            {
                if (element is CircuitCanvasComponent canvasComponent)
                {
                    canvasComponent.MainCanvas.Children.Clear();
                }
            }

            MainCanvas.Children.Clear();
        }

        public void SetElementPosition(FrameworkElement element, Point point)
        {
            var X = point.X;
            var Y = point.Y;

            Canvas.SetLeft(element, X);
            Canvas.SetTop(element, Y);
        }

        public void SetElementPositionOnCenter(FrameworkElement element, Point point)
        {
            var X = point.X - (element.Width / 2);
            var Y = point.Y - (element.Height / 2);

            Canvas.SetLeft(element, X);
            Canvas.SetTop(element, Y);
        }

        private class DraggableCanvasItemEventHandler
        {
            private Point? DragStart { get; set; } = null;
            private Canvas Parent { get; }
            public FrameworkElement Element { get; }

            public DraggableCanvasItemEventHandler(FrameworkElement element, Canvas parent)
            {
                this.Parent = parent;
                this.Element = element;

                Element.MouseLeftButtonDown += (s, args) => this.MouseDown(args.GetPosition((UIElement)s));
                Element.MouseLeftButtonUp += (s, args) => this.MouseUp(args.GetPosition((UIElement)s));

                Element.MouseMove += (s, args) =>
                {
                    if (args.LeftButton == MouseButtonState.Pressed)
                    {
                        this.MouseMove(args.GetPosition(Parent));
                    }
                };
            }

            private void MouseDown(Point position)
            {
                DragStart = position;
                Element.CaptureMouse();
            }

            private void MouseUp(Point position)
            {
                DragStart = null;
                Element.ReleaseMouseCapture();

                if (Element is CircuitCanvasComponent circuitCanvasComponent)
                {
                    circuitCanvasComponent.Component.ComponentView.Position = new Point(Canvas.GetLeft(circuitCanvasComponent), Canvas.GetTop(circuitCanvasComponent));
                }
            }

            private void MouseMove(Point position)
            {
                if (DragStart != null)
                {
                    var newX = position.X - DragStart.Value.X;
                    var newY = position.Y - DragStart.Value.Y;

                    if (newX >= Parent.ActualWidth - Element.ActualWidth || newY >= Parent.ActualHeight - Element.ActualHeight)
                    {
                        Parent.Width = Parent.ActualWidth + PanZoomTransformGroup.SizeIncrement;
                        Parent.Height = Parent.ActualHeight + PanZoomTransformGroup.SizeIncrement;
                    }

                    if (newX > 5 && (newX + (Element.ActualWidth / 2)) <= Parent.ActualWidth)
                    {
                        Canvas.SetLeft(Element, newX);
                    }

                    if (newY > 5 && (newY + (Element.ActualHeight / 2)) <= Parent.ActualHeight)
                    {
                        Canvas.SetTop(Element, newY);
                    }
                }
            }
        }
    }
}
