﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSim.CanvasElements
{
    public enum ZoomType
    {
        ZoomIn,
        ZoomOut
    }
}
