﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace BooleanLogicSim.CanvasElements
{

    public class PanZoomTransformGroup
    {
        public static double ZoomFactor { get; } = 0.05;
        public static double MaxZoomScale { get; } = 10;
        public static double MinZoomScale { get; } = 0.10;
        public static int SizeIncrement { get; } = 250;

        public TransformGroup TransformGroup { get; } = new TransformGroup();

        private MatrixTransform ZoomTransform { get; } = new MatrixTransform();
        private TranslateTransform PanTransform { get; } = new TranslateTransform(0, 0);

        private double CurrentZoomScale => ZoomTransform.Matrix.M11;

        public Point MouseLastPosition { get; set; }

        public PanZoomTransformGroup()
        {
            TransformGroup.Children.Add(PanTransform);
            TransformGroup.Children.Add(ZoomTransform);
        }

        public void ZoomAt(Point point, ZoomType zoomType)
        {
            if (CurrentZoomScale <= MinZoomScale && zoomType == ZoomType.ZoomIn) return;
            if (CurrentZoomScale >= MaxZoomScale && zoomType == ZoomType.ZoomOut) return;

            var zoomMatrix = ZoomTransform.Matrix;

            var centerX = point.X;
            var centerY = point.Y;

            double ZoomScale;

            if (zoomType == ZoomType.ZoomIn)
            {
                ZoomScale = 1 - ZoomFactor;
            }
            else if (zoomType == ZoomType.ZoomOut)
            {
                ZoomScale = 1 + ZoomFactor;
            }
            else
            {
                throw new ArgumentException($"Given ZoomType ({zoomType}) does not have an implementation.");
            }

            zoomMatrix.ScaleAtPrepend(ZoomScale, ZoomScale, centerX, centerY);

            ZoomTransform.Matrix = zoomMatrix;
        }

        public void Pan(Point mousePos)
        {
            Vector panVector = mousePos - MouseLastPosition;

            PanTransform.X += panVector.X;
            PanTransform.Y += panVector.Y;
        }

        public void Reset()
        {
            PanTransform.X = 0;
            PanTransform.Y = 0;

            ZoomTransform.Matrix = new Matrix();
        }
    }
}
