﻿using BooleanLogicSim.Circuit;
using BooleanLogicSim.Circuit.Logic;
using BooleanLogicSim.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSim
{
    public class ComponentsListBoxViewModel
    {
        private IComponentRepository ComponentRepository { get; }

        public ObservableCollection<IComponent> Components { get; set; }

        public ComponentsListBoxViewModel(IComponentRepository componentRepository)
        {
            this.ComponentRepository = componentRepository;
            this.Components = new ObservableCollection<IComponent>(ComponentRepository.RetrieveComponents());
        }

        public void UpdateComponents()
        {
            Components.Clear();
            var components = ComponentRepository.RetrieveComponents();

            foreach (var component in components)
            {
                Components.Add(component);
            }
        }
    }
}
