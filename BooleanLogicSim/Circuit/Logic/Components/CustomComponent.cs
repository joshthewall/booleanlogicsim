﻿using BooleanLogicSim.Circuit.View;
using BooleanLogicSim.Model.LogicCircuits;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace BooleanLogicSim.Circuit.Logic.Components
{

    [Serializable()]
    public class CustomComponent : BaseCircuitComponent, ICloneable
    {
        public static StreamingContextStates ImageUpdateStreamingContextState { get; } = StreamingContextStates.File;
        public static StreamingContextStates ConstructorStreamingContextState { get; } = StreamingContextStates.Other;

        public LogicCircuit LogicCircuit { get; }

        private readonly ICircuitComponentView _componentView;

        public override ICircuitComponentView ComponentView => _componentView;

        public override int MaxInputs => 2;

        public override int MaxOutputs => 1;

        public CustomComponent(LogicCircuit logicCircuit, string plainName, string algebraLabel, string filePath)
        {
            var componentView = new CustomComponentView(plainName, algebraLabel, filePath);

            this._componentView = componentView;
            this.LogicCircuit = logicCircuit;
        }

        public void UpdateImageFilePath(string newPath)
        {
            var compView = this.ComponentView as CustomComponentView;

            compView.FilePath = newPath;
            compView.SetupCustomComponentView(new StreamingContext(ImageUpdateStreamingContextState));
        }

        public override bool ExecuteAction(bool first, bool? second = null)
        {
            var inputs = LogicCircuit.Inputs;

            inputs.ElementAt(0).Value = first;
            inputs.ElementAt(1).Value = second.Value;

            var result = LogicCircuit.EvaluateCircuit();
            if (result.IsError)
            {
                MessageBox.Show($"There has been an unintended error while evaluating a custom component. ({result.IsError})", "Programming Error");
                return false;
            }

            return result.Value;
        }

        public object Clone() => new CustomComponent(this.LogicCircuit,
                                                     this.ComponentView.PlainName,
                                                     this.ComponentView.AlgebraLabel,
                                                     (this.ComponentView as CustomComponentView).FilePath);

        [Serializable()]
        public class CustomComponentView : ICircuitComponentView
        {
            public List<Point> InputPoints { get; set; }

            public Point? OutputPoint { get; set; }

            public string PlainName { get; set; }

            public string AlgebraLabel { get; set; }

            public string FilePath { get; set; }

            public Point? Position { get; set; } = null;

            [NonSerialized()]
            private BitmapImage _bitmapImage;

            public BitmapImage BitmapImage { get => _bitmapImage; set => _bitmapImage = value; }

            public static int LabelHeight { get; } = 64;
            public static int LabelWidth { get; } = 92;
            public static int LabelFontSize { get; } = 26;

            public static int ImageWidth { get; } = 100;
            public static int ImageHeight { get; } = 50;

            public CustomComponentView(string plainName, string algebraLabel, string filePath)
            {
                this.PlainName = plainName;
                this.AlgebraLabel = algebraLabel;
                this.FilePath = filePath;

                SetupCustomComponentView(new StreamingContext(ConstructorStreamingContextState));
            }

            [OnDeserialized()]
            public void SetupCustomComponentView(StreamingContext context)
            {
                // Relative points
                this.InputPoints = new List<Point>() { new Point(0, 0.25), new Point(0, 0.75) };
                this.OutputPoint = new Point(1, 0.5);

                var fromConstructor = context.State == ConstructorStreamingContextState;

                var outputPoint = this.OutputPoint.Value;

                int totalWidth;
                int totalHeight;

                if (this.FilePath == null)
                {
                    if (!fromConstructor) MessageBox.Show($"Couldn't find the image file for {PlainName} - defaulted to label", "IO Error");

                    totalWidth = LabelWidth;
                    totalHeight = LabelHeight;
                }
                else
                {
                    try
                    {
                        var bitmapImage = new BitmapImage();

                        bitmapImage.BeginInit();
                        bitmapImage.UriSource = new Uri(this.FilePath);
                        bitmapImage.EndInit();

                        this.BitmapImage = bitmapImage;

                        totalWidth = ImageWidth;
                        totalHeight = ImageHeight;
                    }
                    catch
                    {
                        if (!fromConstructor) MessageBox.Show($"There was an error reading the image file for {PlainName} - defaulted to label", "IO Error");

                        totalWidth = LabelWidth;
                        totalHeight = LabelHeight;
                    }
                }

                this.InputPoints = this.InputPoints.Select(point => new Point(point.X * totalWidth, point.Y * totalHeight)).ToList();
                this.OutputPoint = new Point(outputPoint.X * totalWidth, outputPoint.Y * totalHeight);
            }

            public FrameworkElement GetElement()
            {
                if (BitmapImage == null)
                {
                    return new Label
                    {
                        Content = PlainName,
                        Padding = new Thickness(8),
                        VerticalContentAlignment = VerticalAlignment.Center,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        FontSize = LabelFontSize,
                        Height = LabelHeight,
                        Width = LabelWidth,
                        BorderThickness = new Thickness(2),
                        BorderBrush = Brushes.Orange,
                        Cursor = Cursors.Hand
                    };
                }

                var image = new Image()
                {
                    Source = BitmapImage,
                    Width = ImageWidth,
                    Height = ImageHeight,
                    Cursor = Cursors.Hand
                };

                return image;
            }
        }
    }
}
