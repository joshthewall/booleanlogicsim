﻿using BooleanLogicSim.Circuit.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace BooleanLogicSim.Circuit.Logic.Components
{
    [Serializable()]
    public class Input : BaseCircuitComponent
    {
        private bool _value = true;

        public bool Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;

                (this.ComponentView as InputView).UpdateLabelValue(_value);
            }
        }

        [NonSerialized()]
        public static string ComponentName = "IN";

        public override ICircuitComponentView ComponentView { get; } = new InputView();

        public override int MaxInputs => 0;
        public override int MaxOutputs => int.MaxValue;

        [OnDeserialized()]
        public void SetupInput(StreamingContext context)
        {
            (this.ComponentView as InputView).UpdateLabelValue(_value);
        }

        public override bool Equals(object obj)
        {
            var input = obj as Input;
            return input != null && this.ComponentView.AlgebraLabel == input.ComponentView.AlgebraLabel;
        }

        public override bool ExecuteAction(bool first, bool? second = null)
        {
            return Value;
        }

        public override int GetHashCode()
        {
            var hashCode = 2006718823;
            hashCode = hashCode * -1521134295 + Value.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<ICircuitComponentView>.Default.GetHashCode(ComponentView);
            hashCode = hashCode * -1521134295 + MaxInputs.GetHashCode();
            hashCode = hashCode * -1521134295 + MaxOutputs.GetHashCode();
            return hashCode;
        }

        [Serializable()]
        private class InputView : ICircuitComponentView
        {
            public List<Point> InputPoints { get; } = null;
            public Point? OutputPoint { get; private set; }

            public Point? Position { get; set; } = null;

            public string PlainName => ComponentName;

            private string _algebraLabel;

            public string AlgebraLabel
            {
                get => _algebraLabel;

                set
                {
                    _algebraLabel = value;

                    NameLabel.Content = _algebraLabel;
                }
            }

            public static int ValueLabelHeight { get; } = 36;
            public static int ValueLabelWidth { get; } = 36;
            public static int ValueLabelFontSize { get; } = 23;

            public static int NameLabelHeight { get; } = 31;

            [NonSerialized()]
            private Label _valueLabel = new Label
            {
                Content = 1,
                Padding = new Thickness(0),
                VerticalContentAlignment = VerticalAlignment.Center,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                FontSize = ValueLabelFontSize,
                Height = ValueLabelHeight,
                Width = ValueLabelWidth,
                BorderThickness = new Thickness(2),
                BorderBrush = Brushes.Green,
                Cursor = Cursors.Hand
            };

            private Label ValueLabel => _valueLabel;

            [NonSerialized()]
            private Label _nameLabel = new Label
            {
                VerticalContentAlignment = VerticalAlignment.Center,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                FontSize = ValueLabelFontSize / 1.5,
                Width = ValueLabelWidth,
                Height = NameLabelHeight,
                Padding = new Thickness(0),
                ClipToBounds = false,
                IsHitTestVisible = false
            };

            private Label NameLabel => _nameLabel;

            [NonSerialized()]
            private StackPanel _stackPanel = new StackPanel();

            private StackPanel StackPanel => _stackPanel;

            public InputView()
            {
                SetupInputView(new StreamingContext());
            }

            [OnDeserialized()]
            public void SetupInputView(StreamingContext context)
            {
                this._valueLabel = this._valueLabel ?? new Label
                {
                    Padding = new Thickness(0),
                    VerticalContentAlignment = VerticalAlignment.Center,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    FontSize = ValueLabelFontSize,
                    Height = ValueLabelHeight,
                    Width = ValueLabelWidth,
                    BorderThickness = new Thickness(2),
                    BorderBrush = Brushes.Green,
                    Cursor = Cursors.Hand
                };

                this._nameLabel = this._nameLabel ?? new Label
                {
                    VerticalContentAlignment = VerticalAlignment.Center,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    FontSize = ValueLabelFontSize / 1.5,
                    Width = ValueLabelWidth,
                    Height = 31,
                    Padding = new Thickness(0),
                    ClipToBounds = false,
                    Content = this.AlgebraLabel,
                    IsHitTestVisible = false
                };

                this._stackPanel = this._stackPanel ?? new StackPanel();

                StackPanel.Children.Add(NameLabel);
                StackPanel.Children.Add(ValueLabel);

                StackPanel.Width = NameLabel.Width;
                StackPanel.Height = ValueLabel.Height + NameLabel.Height;

                this.OutputPoint = new Point(ValueLabel.Width, NameLabel.Height + ValueLabel.Height / 2);
            }

            public void UpdateLabelValue(bool value)
            {
                ValueLabel.Content = value ? 1 : 0;

                if (value)
                {
                    ValueLabel.BorderBrush = Brushes.Green;
                }
                else
                {
                    ValueLabel.BorderBrush = Brushes.Red;
                }
            }

            public FrameworkElement GetElement()
            {
                (StackPanel.Parent as Canvas)?.Children.Remove(StackPanel);

                return StackPanel;
            }
        }
    }


}
