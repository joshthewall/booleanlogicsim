﻿using BooleanLogicSim.Circuit.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BooleanLogicSim.Circuit.Logic.Components
{
    [Serializable()]
    public class OrGate : BaseCircuitComponent
    {
        public static string ComponentName { get; } = "OR";
        public static string AlgebraLabel { get; } = "+";

        public override ICircuitComponentView ComponentView { get; } = new ImageComponentView(ComponentName,
                                                                                           AlgebraLabel,
                                                                                           new List<Point>() { new Point(0, 0.25), new Point(0, 0.75) },
                                                                                           new Point(1, 0.5));

        public override int MaxInputs => 2;

        public override int MaxOutputs => 1;

        public override bool ExecuteAction(bool first, bool? second = null) => first || second.Value;
    }
}
