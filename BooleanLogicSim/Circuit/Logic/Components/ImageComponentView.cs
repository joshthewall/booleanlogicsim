﻿using BooleanLogicSim.Circuit.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Xml.Serialization;

namespace BooleanLogicSim.Circuit.Logic.Components
{
    [Serializable()]
    public class ImageComponentView : ICircuitComponentView
    {
        public List<Point> InputPoints { get; private set; }

        public Point? OutputPoint { get; private set; }

        public string AlgebraLabel { get; set; }

        public string PlainName { get; }

        public Point? Position { get; set; } = null;

        [NonSerialized()]
        private BitmapImage _bitmapImage;

        [NonSerialized()]
        private Image _imageElement;

        public ImageComponentView(string plainName, string algebraLabel, List<Point> relativeInputPoints, Point relativeOutputPoint)
        {
            var bitmapImage = new BitmapImage();

            bitmapImage.BeginInit();
            bitmapImage.UriSource = new Uri($"pack://application:,,,/Data/ComponentImages/{plainName}.png");
            bitmapImage.EndInit();

            this._bitmapImage = bitmapImage;

            this._imageElement = new Image()
            {
                Source = bitmapImage,
                Height = bitmapImage.Height,
                Width = bitmapImage.Width,
                Cursor = Cursors.Hand
            };

            this.PlainName = plainName;
            this.AlgebraLabel = algebraLabel;

            this.InputPoints = relativeInputPoints.Select(p => new Point(p.X * bitmapImage.Width, p.Y * bitmapImage.Height)).ToList();
            this.OutputPoint = new Point(relativeOutputPoint.X * bitmapImage.Width, relativeOutputPoint.Y * bitmapImage.Height);
        }

        [OnDeserialized()]
        public void SetupImageComponentView(StreamingContext context)
        {
            var bitmapImage = new BitmapImage();

            bitmapImage.BeginInit();
            bitmapImage.UriSource = new Uri($"pack://application:,,,/Data/ComponentImages/{this.PlainName}.png");
            bitmapImage.EndInit();

            this._bitmapImage = bitmapImage;

            this._imageElement = new Image()
            {
                Source = bitmapImage,
                Height = bitmapImage.Height,
                Width = bitmapImage.Width,
                Cursor = Cursors.Hand
            };
        }

        public FrameworkElement GetElement() => _imageElement;
    }
}
