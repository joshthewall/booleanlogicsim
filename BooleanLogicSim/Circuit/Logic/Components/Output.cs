﻿using BooleanLogicSim.Circuit.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace BooleanLogicSim.Circuit.Logic.Components
{
    [Serializable()]
    public class Output : BaseCircuitComponent
    {
        public override int MaxInputs => 1;
        public override int MaxOutputs => 0;

        [NonSerialized()]
        public static string ComponentName = "OUT";

        public override ICircuitComponentView ComponentView { get; } = new OutputView();

        public override bool ExecuteAction(bool first, bool? second = null)
        {
            return first;
        }

        [Serializable()]
        private class OutputView : ICircuitComponentView
        {
            public List<Point> InputPoints { get; } = new List<Point>() { new Point(0, LabelHeight / 2) };
            public Point? OutputPoint { get; } = null;

            public Point? Position { get; set; } = null;

            public static int LabelHeight { get; } = 36;
            public static int LabelWidth { get; } = 64;
            public static int LabelFontSize { get; } = 26;

            public string PlainName => ComponentName;

            public string AlgebraLabel { get; set; }

            public FrameworkElement GetElement() => new Label
            {
                Content = "OUT",
                Padding = new Thickness(0),
                VerticalContentAlignment = VerticalAlignment.Center,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                FontSize = LabelFontSize,
                Height = LabelHeight,
                Width = LabelWidth,
                BorderThickness = new Thickness(2),
                BorderBrush = Brushes.Blue,
                Cursor = Cursors.Hand
            };
        }
    }
}
