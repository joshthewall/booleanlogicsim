﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using BooleanLogicSim.Circuit.View;

namespace BooleanLogicSim.Circuit.Logic
{
    [DataContract]
    [Serializable()]
    public abstract class BaseCircuitComponent : ICircuitComponent
    {
        public abstract ICircuitComponentView ComponentView { get; }

        [IgnoreDataMember()]
        IComponentView IComponent.ComponentView => this.ComponentView;

        public abstract int MaxInputs { get; }
        public abstract int MaxOutputs { get; }

        [IgnoreDataMember()]
        public List<ICircuitComponent> ConnectedComponentsIn { get; private set; } = new List<ICircuitComponent>();

        [IgnoreDataMember()]
        public List<ICircuitComponent> ConnectedComponentsOut { get; private set; } = new List<ICircuitComponent>();

        public abstract bool ExecuteAction(bool first, bool? second = null);

        [OnDeserialized]
        private void ConstructComponent(StreamingContext context)
        {
            if (this.ConnectedComponentsIn != null || this.ConnectedComponentsOut != null) return;

            this.ConnectedComponentsIn = new List<ICircuitComponent>();
            this.ConnectedComponentsOut = new List<ICircuitComponent>();
        }

        public bool Connect(ICircuitComponent connectingComponent)
        {
            var connected = this.ConnectComponent(connectingComponent);

            return connected && ((connectingComponent as BaseCircuitComponent)?.ConnectToComponent(this) ?? true);
        }

        // Internal methods needed to prevent infinite recursion when calling each other

        private bool ConnectComponent(ICircuitComponent connectingComponent)
        {
            if (this.ConnectedComponentsIn.Count >= MaxInputs) return false;

            this.ConnectedComponentsIn.Add(connectingComponent);

            return true;
        }

        public bool Disconnect(ICircuitComponent disconnectingComponent)
        {
            var disconnected = this.DisconnectComponent(disconnectingComponent);

            return disconnected && ((disconnectingComponent as BaseCircuitComponent)?.DisconnectFromComponent(this) ?? true);
        }

        private bool DisconnectComponent(ICircuitComponent disconnectingComponent) => this.ConnectedComponentsIn.Remove(disconnectingComponent);

        public bool ConnectTo(ICircuitComponent component)
        {
            var connected = this.ConnectToComponent(component);

            return connected && ((component as BaseCircuitComponent)?.ConnectComponent(this) ?? true);
        }

        private bool ConnectToComponent(ICircuitComponent component)
        {
            if (this.ConnectedComponentsOut.Count >= MaxOutputs) return false;

            this.ConnectedComponentsOut.Add(component);

            return true;
        }

        public bool DisconnectFrom(ICircuitComponent component)
        {
            var disconnected = this.DisconnectFromComponent(component);

            return disconnected && ((component as BaseCircuitComponent)?.Disconnect(this) ?? true);
        }

        private bool DisconnectFromComponent(ICircuitComponent component) => this.ConnectedComponentsOut.Remove(component);
    }
}
