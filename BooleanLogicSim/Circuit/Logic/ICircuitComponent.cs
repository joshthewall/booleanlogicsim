﻿using BooleanLogicSim.Circuit.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSim.Circuit.Logic
{
    public interface ICircuitComponent : IComponent
    {
        new ICircuitComponentView ComponentView { get; }

        List<ICircuitComponent> ConnectedComponentsIn { get; }
        List<ICircuitComponent> ConnectedComponentsOut { get; }

        bool ConnectTo(ICircuitComponent component);
        bool DisconnectFrom(ICircuitComponent component);
        bool Connect(ICircuitComponent connectingComponent);
        bool Disconnect(ICircuitComponent connectingComponent);
    }
}
