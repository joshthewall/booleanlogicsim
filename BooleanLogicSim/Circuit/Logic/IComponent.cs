﻿using BooleanLogicSim.Circuit.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSim.Circuit.Logic
{
    public interface IComponent
    {
        IComponentView ComponentView { get; }

        int MaxInputs { get; }
        int MaxOutputs { get; }

        bool ExecuteAction(bool first, bool? second = null);
    }
}
