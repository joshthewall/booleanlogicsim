﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Xml.Serialization;

namespace BooleanLogicSim.Circuit.View
{
    public interface IComponentView
    {
        string PlainName { get; }
        string AlgebraLabel { get; set; }
    }
}
