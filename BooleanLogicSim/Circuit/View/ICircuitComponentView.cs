﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BooleanLogicSim.Circuit.View
{
    public interface ICircuitComponentView : IComponentView
    {
        List<Point> InputPoints { get; }
        Point? OutputPoint { get; }

        Point? Position { get; set; }

        FrameworkElement GetElement();
    }
}
