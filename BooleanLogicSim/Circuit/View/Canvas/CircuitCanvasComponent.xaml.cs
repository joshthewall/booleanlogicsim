﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using CanvasControl = System.Windows.Controls.Canvas;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BooleanLogicSim.Extensions;
using BooleanLogicSim.Model;
using BooleanLogicSim.Model.LogicCircuits;
using BooleanLogicSim.Circuit.Logic;
using BooleanLogicSim.Circuit.Logic.Components;

namespace BooleanLogicSim.Circuit.View.Canvas
{
    public partial class CircuitCanvasComponent : UserControl
    {
        public static int InputOutputZIndex { get; } = 100;
        public static string ConnectionDataObjectIdentifier { get; } = "ConnectionDataObject";

        public static Brush InputConnectedBrush { get; } = Brushes.ForestGreen;
        public static Brush InputUnconnectedBrush { get; } = Brushes.Aquamarine;

        public CanvasControl ParentCanvas => this.Parent as System.Windows.Controls.Canvas;

        private LogicCircuit LogicCircuit { get; set; }

        public Line PotentialConnectionLine { get; private set; }

        public ICircuitComponent Component { get; private set; }

        public bool CanAcceptConnection => this.Component.ConnectedComponentsIn.Count <= this.Component.MaxInputs;
        public bool HasConnectionsOut => this.Component.ConnectedComponentsOut.Count > 0;

        public List<InputRegion> InputRegions { get; } = new List<InputRegion>();
        public List<InputRegion> ConnectedInputRegions => InputRegions.Where(region => region.ConnectionPointUsed).ToList();
        public List<CircuitCanvasComponent> ConnectedInputComponents => ConnectedInputRegions.Select(region => region.ConnectedComponent).ToList();

        public OutputRegion OutputRegion { get; set; }

        public List<CircuitCanvasComponent> ConnectedOutputComponents =>
            OutputRegion?.ConnectionsOut
                         .Where(connection => connection.ComponentConnectedTo != null)
                         .Select(connection => connection.ComponentConnectedTo).ToList();

        private FrameworkElement MainElement { get; set; }

        private MainWindow MainWindow { get; set; }

        public bool IsDraggingConnection { get; private set; }

        public CircuitCanvasComponent()
        {
            InitializeComponent();
        }

        public void SetComponent(ICircuitComponent component)
        {
            MainCanvas.Children.Clear();

            Component = component;

            var mainElement = component.ComponentView.GetElement();
            MainElement = mainElement;

            CanvasControl.SetLeft(mainElement, 0);
            CanvasControl.SetTop(mainElement, 0);

            MainCanvas.Children.Add(mainElement);

            SetupInputPoints(component.ComponentView, MainCanvas);
            SetupOutputPoint(component.ComponentView, MainCanvas);
            SetupContextMenu();

            this.Width = CanvasControl.GetLeft(mainElement) + mainElement.Width;
            this.Height = CanvasControl.GetTop(mainElement) + mainElement.Height;
        }

        public void SetLogicCircuit(LogicCircuit logicCircuit)
        {
            this.LogicCircuit = logicCircuit;
        }

        public void SetMainWindow(MainWindow mainWindow)
        {
            this.MainWindow = mainWindow;
        }

        public Point? GetOutputPoint()
            => Component.ComponentView.OutputPoint?.GetAbsoluteCanvasPoint(this);

        private void SetupContextMenu()
        {
            var contextMenu = new ContextMenu();

            var deleteMenuItem = new MenuItem()
            {
                Header = "Delete component"
            };

            deleteMenuItem.Click += (s, args) =>
            {
                DeleteComponent();
            };

            if (this.Component is Input input)
            {
                var changeValueMenuItem = new MenuItem()
                {
                    Header = "Change value"
                };

                changeValueMenuItem.Click += (s, args) =>
                {
                    foreach (var element in ParentCanvas.Children)
                    {
                        if (element is CircuitCanvasComponent canvasComponent
                            && canvasComponent.Component is Input inputComp
                            && inputComp.Equals(input))
                        {
                            inputComp.Value = !inputComp.Value;
                        }
                    }
                };

                contextMenu.Items.Add(changeValueMenuItem);
            }

            contextMenu.Items.Add(deleteMenuItem);

            this.ContextMenu = contextMenu;
        }

        public void DeleteComponent()
        {
            ConnectedInputComponents.ForEach(compIn =>
            {
                this.Disconnect(compIn);
            });

            OutputRegion?.ConnectionsOut.Select(connection => connection.ComponentConnectedTo)
                                       .ToList()
                                       .ForEach(compOut =>
                                       {
                                           this.DisconnectFrom(compOut);
                                       });

            if (this.Component is Output)
            {
                MainWindow.EnableOutputCreation();
            }

            LogicCircuit.RemoveComponent(this.Component);
            ParentCanvas.Children.Remove(this);
        }

        private void SetupInputPoints(ICircuitComponentView component, CanvasControl canvas)
        {
            if (component.InputPoints == null) return;

            foreach (var pos in component.InputPoints)
            {
                var inputRectangle = new Rectangle()
                {
                    Width = 20,
                    Height = 20,
                    Fill = InputUnconnectedBrush,
                    Cursor = Cursors.Hand
                };

                var inputRegion = new InputRegion(inputRectangle, pos);

                inputRectangle.PreviewMouseLeftButtonDown += (s, args) =>
                {
                    var connectedComponent = inputRegion.ConnectedComponent;

                    if (connectedComponent == null) return;

                    this.Disconnect(connectedComponent);

                    connectedComponent.StartConnection();

                    args.Handled = true;
                };

                inputRectangle.PreviewDrop += (s, args) =>
                {
                    if (!args.Data.GetDataPresent(ConnectionDataObjectIdentifier)) return;

                    var connectingComponent = (CircuitCanvasComponent)args.Data.GetData(ConnectionDataObjectIdentifier);
                    if (connectingComponent == this) return;
                    if (ConnectedInputComponents.Contains(connectingComponent)) return;
                    if (ConnectedInputRegions.Select(region => region.RegionRectangle).Contains(inputRectangle)) return;
                    if (ConnectedInputComponents.Count >= this.Component.MaxInputs) return;

                    this.Connect(connectingComponent, inputRegion);

                    args.Handled = true;
                };

                inputRectangle.PreviewDragOver += (s, args) =>
                {
                    if (!args.Data.GetDataPresent(ConnectionDataObjectIdentifier)) return;

                    var connectingComponent = (CircuitCanvasComponent)args.Data.GetData(ConnectionDataObjectIdentifier);
                    if (connectingComponent == this) return;
                    if (ConnectedInputComponents.Contains(connectingComponent)) return;
                    if (ConnectedInputRegions.Select(region => region.RegionRectangle).Contains(inputRectangle)) return;
                    if (ConnectedInputComponents.Count >= this.Component.MaxInputs) return;

                    connectingComponent.SetConnectionLinePosition(connectingComponent.PotentialConnectionLine, pos.GetAbsoluteCanvasPoint(this));

                    inputRectangle.Fill = InputConnectedBrush;

                    args.Handled = true;
                };

                inputRectangle.PreviewDragLeave += (s, args) =>
                {
                    var connectedInputRegions = this.ConnectedInputRegions.Select(region => region.RegionRectangle);
                    if (connectedInputRegions.Contains(inputRectangle)) return;

                    inputRectangle.Fill = InputUnconnectedBrush;
                };

                CanvasControl.SetLeft(inputRectangle, pos.X - inputRectangle.Width / 2);
                CanvasControl.SetTop(inputRectangle, pos.Y - inputRectangle.Height / 2);
                CanvasControl.SetZIndex(inputRectangle, InputOutputZIndex);

                this.InputRegions.Add(inputRegion);

                canvas.Children.Add(inputRectangle);
            }
        }

        private void SetupOutputPoint(ICircuitComponentView component, CanvasControl canvas)
        {
            if (component.OutputPoint == null) return;

            var outputPos = (Point)component.OutputPoint;

            var outputRectangle = new Rectangle()
            {
                Width = 20,
                Height = 20,
                Fill = Brushes.BlueViolet,
                Cursor = Cursors.Hand,
                IsHitTestVisible = true
            };

            var outputRegion = new OutputRegion(outputRectangle, outputPos);

            this.OutputRegion = outputRegion;

            outputRectangle.PreviewMouseDown += (s, args) =>
            {
                if (OutputRegion.ConnectionsOut.Count >= this.Component.MaxOutputs)
                {
                    var firstConnectedComponentOut = OutputRegion.ConnectionsOut.Select(connection => connection.ComponentConnectedTo).FirstOrDefault();
                    if (firstConnectedComponentOut == null) return;

                    this.DisconnectFrom(firstConnectedComponentOut);
                }

                this.StartConnection();

                args.Handled = true;
            };

            CanvasControl.SetLeft(outputRectangle, outputPos.X - outputRectangle.Width / 2);
            CanvasControl.SetTop(outputRectangle, outputPos.Y - outputRectangle.Height / 2);
            CanvasControl.SetZIndex(outputRectangle, InputOutputZIndex);

            canvas.Children.Add(outputRectangle);
        }

        private Line CreateConnectionLine()
        {
            var outputPoint = OutputRegion.RelativePoint.GetAbsoluteCanvasPoint(this);

            var line = new Line()
            {
                Stroke = Brushes.Black,
                StrokeThickness = 5,
                IsHitTestVisible = false,
                X1 = outputPoint.X,
                Y1 = outputPoint.Y
            };

            return line;
        }

        private void StartConnection()
        {
            this.IsDraggingConnection = true;

            PotentialConnectionLine = CreateConnectionLine();

            CanvasControl.SetZIndex(PotentialConnectionLine, 2);
            ParentCanvas.Children.Add(PotentialConnectionLine);

            var data = new DataObject(ConnectionDataObjectIdentifier, this);

            var effect = DragDrop.DoDragDrop(this, data, DragDropEffects.Copy);

            if (effect == DragDropEffects.None)
            {
                this.CancelConnection();
            }

            this.IsDraggingConnection = false;
        }

        public void CancelConnection()
        {
            ParentCanvas.Children.Remove(PotentialConnectionLine);
            PotentialConnectionLine = null;
        }

        public void ConnectTo(CircuitCanvasComponent componentConnectingTo, InputRegion inputRegion)
        {
            this.ConnectToComponent(componentConnectingTo, inputRegion);
            componentConnectingTo.ConnectComponent(this, inputRegion);

            this.Component.ConnectTo(componentConnectingTo.Component);
        }

        private void ConnectToComponent(CircuitCanvasComponent componentConnectingTo, InputRegion inputRegion)
        {
            var alreadyConnected = OutputRegion.ConnectionsOut.Any(conn => conn.ComponentConnectedTo.Equals(componentConnectingTo));

            if (inputRegion.ConnectionLine != null || alreadyConnected) return;

            var connectionLine = PotentialConnectionLine ?? CreateConnectionLine();

            if (!ParentCanvas.Children.Contains(connectionLine))
            {
                ParentCanvas.Children.Add(connectionLine);
            }

            inputRegion.ConnectionLine = connectionLine;

            OutputRegion.ConnectionsOut.Add(new OutputConnection(componentConnectingTo, inputRegion, connectionLine));

            PotentialConnectionLine = null;
        }

        public void DisconnectFrom(CircuitCanvasComponent componentConnectedTo)
        {
            this.DisconnectFromComponent(componentConnectedTo);
            componentConnectedTo.DisconnectComponent(this);

            this.Component.DisconnectFrom(componentConnectedTo.Component);
        }

        private void DisconnectFromComponent(CircuitCanvasComponent componentConnectedTo)
        {
            var connection = OutputRegion.ConnectionsOut.FirstOrDefault(conn => conn.ComponentConnectedTo == componentConnectedTo);
            if (connection == null) return;

            ParentCanvas.Children.Remove(connection.ConnectionLine);

            OutputRegion.ConnectionsOut.Remove(connection);
        }

        public void Connect(CircuitCanvasComponent connectingComponent, InputRegion inputRegion)
        {
            this.ConnectComponent(connectingComponent, inputRegion);
            connectingComponent.ConnectToComponent(this, inputRegion);

            this.Component.Connect(connectingComponent.Component);
        }

        private void ConnectComponent(CircuitCanvasComponent connectingComponent, InputRegion inputRegion)
        {
            inputRegion.ConnectedComponent = connectingComponent;
            inputRegion.RegionRectangle.Fill = InputConnectedBrush;

            var alreadyConnected = this.Component.ConnectedComponentsIn.Contains(connectingComponent.Component);

            if (alreadyConnected) return;
        }

        public void Disconnect(CircuitCanvasComponent connectingComponent)
        {
            this.DisconnectComponent(connectingComponent);
            connectingComponent.DisconnectFromComponent(this);

            this.Component.Disconnect(connectingComponent.Component);
        }

        private void DisconnectComponent(CircuitCanvasComponent connectingComponent)
        {
            var connectedInputRegion = InputRegions.FirstOrDefault(region => region.ConnectedComponent == connectingComponent);
            if (connectedInputRegion == null) return;

            connectedInputRegion.RegionRectangle.Fill = InputUnconnectedBrush;

            connectedInputRegion.ConnectionLine = null;
            connectedInputRegion.ConnectedComponent = null;
        }

        public void SetConnectionLinePosition(Line line, Point pos)
        {
            var outputPoint = this.GetOutputPoint();
            if (!outputPoint.HasValue) return;

            line.X1 = outputPoint.Value.X;
            line.Y1 = outputPoint.Value.Y;

            line.X2 = pos.X;
            line.Y2 = pos.Y;
        }

        

        private void CircuitComponent_LayoutUpdated(object sender, EventArgs e)
        {            
            if (this.Component.ComponentView.OutputPoint == null
                || this.OutputRegion == null
                || this.IsDraggingConnection) return;


            foreach (var connection in OutputRegion.ConnectionsOut)
            {
                var component = connection.ComponentConnectedTo;
                var inputRegion = connection.InputRegionConnectedTo;
                var line = connection.ConnectionLine;

                this.SetConnectionLinePosition(line, inputRegion.RelativePoint.GetAbsoluteCanvasPoint(component));
            }
        }

        public override string ToString()
        {
            return this.Component.ComponentView.PlainName;
        }
    }

    public class ConnectionEventArgs : EventArgs
    {
        public ICircuitComponent ConnectionComponent { get; }

        public ConnectionEventArgs(ICircuitComponent connectionComponent)
        {
            this.ConnectionComponent = connectionComponent;
        }
    }
}
