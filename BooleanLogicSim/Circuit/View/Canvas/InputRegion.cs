﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;

namespace BooleanLogicSim.Circuit.View.Canvas
{
    public class InputRegion
    {
        public CircuitCanvasComponent ConnectedComponent { get; set; }

        public Rectangle RegionRectangle { get; private set; }

        public Point RelativePoint { get; private set; }

        public Line ConnectionLine { get; set; }

        public bool ConnectionPointUsed => ConnectedComponent != null;

        public InputRegion(Rectangle regionRectangle, Point relativePoint, Line connectionLine = null)
        {
            RegionRectangle = regionRectangle;
            RelativePoint = relativePoint;
            ConnectionLine = connectionLine;
        }
    }
}
