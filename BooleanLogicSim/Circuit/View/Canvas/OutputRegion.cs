﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;

namespace BooleanLogicSim.Circuit.View.Canvas
{
    public class OutputRegion
    {
        public List<OutputConnection> ConnectionsOut { get; } = new List<OutputConnection>();

        public Rectangle RegionRectangle { get; set; }

        public Point RelativePoint { get; private set; }

        public bool IsConnectedOut => ConnectionsOut != null;

        public OutputRegion(Rectangle regionRectangle, Point relativePoint)
        {
            RegionRectangle = regionRectangle;
        }
    }

    public class OutputConnection
    {
        public CircuitCanvasComponent ComponentConnectedTo { get; set; }
        public Line ConnectionLine { get; set; }
        public InputRegion InputRegionConnectedTo { get; set; }

        public OutputConnection(CircuitCanvasComponent componentConnectedTo, InputRegion inputRegionConnectedTo, Line connectionLine)
        {
            ComponentConnectedTo = componentConnectedTo;
            InputRegionConnectedTo = inputRegionConnectedTo;
            ConnectionLine = connectionLine;
        }
    }
}
