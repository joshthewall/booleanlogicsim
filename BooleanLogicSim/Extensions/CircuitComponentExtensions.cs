﻿using BooleanLogicSim.Circuit.Logic;
using BooleanLogicSim.Model.BooleanAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSim.Extensions
{
    public static class CircuitComponentExtensions
    {
        public static int Precedence(this ICircuitComponent comp) => TokenTypes.OperatorPrecedences[comp.ComponentView.AlgebraLabel];
    }
}
