﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace BooleanLogicSim.Extensions
{
    public static class PointExtensions
    {
        public static Point GetAbsoluteCanvasPoint(this Point relativePoint, UIElement element)
            => new Point(Canvas.GetLeft(element) + relativePoint.X, Canvas.GetTop(element) + relativePoint.Y);
    }
}
