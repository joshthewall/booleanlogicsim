﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.VisualBasic;
using BooleanLogicSim.Circuit;
using BooleanLogicSim.Circuit.Logic.Components;
using BooleanLogicSim.Circuit.Logic;

namespace BooleanLogicSim.Data
{
    public class DefaultComponentRepository : IComponentRepository
    {
        private CustomComponentRepository CustomComponentRepository { get; set; }

        private Dictionary<string, Func<IComponent>> DefaultComponentsByName { get; } = new Dictionary<string, Func<IComponent>>
        {
            { Input.ComponentName,   () => new Input()  },
            { Output.ComponentName,  () => new Output() },
            { NotGate.ComponentName, () => new NotGate() },
            { AndGate.ComponentName, () => new AndGate() },
            { OrGate.ComponentName,  () => new OrGate() }
        };

        private Dictionary<string, Func<IComponent>> DefaultComponentsBySymbol { get; } = new Dictionary<string, Func<IComponent>>()
        {
            { NotGate.AlgebraLabel, () => new NotGate() },
            { AndGate.AlgebraLabel, () => new AndGate() },
            { OrGate.AlgebraLabel,  () => new OrGate() }
        };

        public DefaultComponentRepository(CustomComponentRepository customComponentRepository)
        {
            this.CustomComponentRepository = customComponentRepository;
        }

        public IComponent GetComponentByName(string name)
        {
            DefaultComponentsByName.TryGetValue(name, out var defaultComponent);
            IComponent customComponent;

            if (defaultComponent != null)
            {
                return defaultComponent.Invoke();
            }
            else if ((customComponent = CustomComponentRepository.GetComponentByName(name)) != null)
            {
                return customComponent;
            }
            else
            {
                return null;
            }
        }

        public IComponent GetComponentBySymbol(string symbol)
        {
            DefaultComponentsBySymbol.TryGetValue(symbol, out var defaultComponent);

            var customComponent = CustomComponentRepository.GetComponentBySymbol(symbol);

            if (defaultComponent != null)
            {
                return defaultComponent.Invoke();
            }
            else if (customComponent != null)
            {
                return customComponent;
            }
            else
            {
                var input = (Input)DefaultComponentsByName[Input.ComponentName].Invoke();
                input.ComponentView.AlgebraLabel = symbol;

                return input;
            }
        }

        public List<string> RetrieveComponentNames() => DefaultComponentsByName.Keys
                                                                .Concat(CustomComponentRepository.RetrieveComponentNames())
                                                                .ToList();

        public List<string> RetrieveComponentSymbols() => DefaultComponentsBySymbol.Keys
                                                                .Concat(CustomComponentRepository.RetrieveComponentSymbols())
                                                                .ToList();

        public List<IComponent> RetrieveComponents() => DefaultComponentsByName.Values
                                                                .Select(func => func.Invoke())
                                                                .Concat(CustomComponentRepository.RetrieveComponents())
                                                                .ToList();
    }
}
