﻿using BooleanLogicSim.Circuit;
using BooleanLogicSim.Circuit.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSim.Data
{
    public interface IComponentRepository
    {
        IComponent GetComponentByName(string name);
        IComponent GetComponentBySymbol(string symbol);
        List<IComponent> RetrieveComponents();
        List<string> RetrieveComponentNames();
        List<string> RetrieveComponentSymbols();
    }
}
