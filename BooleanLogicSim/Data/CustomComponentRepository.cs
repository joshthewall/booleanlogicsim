﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using BooleanLogicSim.Circuit;
using BooleanLogicSim.Circuit.Logic;
using BooleanLogicSim.Circuit.Logic.Components;
using BooleanLogicSim.Model;
using BooleanLogicSim.Model.BooleanAlgebra;
using BooleanLogicSim.Model.LogicCircuits;
using static BooleanLogicSim.Circuit.Logic.Components.CustomComponent;

namespace BooleanLogicSim.Data
{
    [Serializable()]
    public class CustomComponentRepository : IComponentRepository
    {
        private List<CustomComponent> Components { get; } = new List<CustomComponent>();

        public CustomComponentRepository()
        {
            this.AddComponent(CreateXorComponent());
        }

        public bool AddComponent(CustomComponent component)
        {
            if (this.ContainsComponentWithSymbol(component.ComponentView.AlgebraLabel) ||
                this.ContainsComponentWithPlainName(component.ComponentView.PlainName))
            {
                return false;
            }

            this.Components.Add(component);

            TokenTypes.Tokens[component.ComponentView.AlgebraLabel] = TokenType.BinaryOperator;
            TokenTypes.OperatorPrecedences[component.ComponentView.AlgebraLabel] = 2;

            return true;
        }

        public Result<bool> AddComponents(List<CustomComponent> customComponents)
        {
            foreach (var comp in customComponents)
            {
                var containsPlainName = this.ContainsComponentWithPlainName(comp.ComponentView.PlainName);
                var containsAlgebraLabel = this.ContainsComponentWithSymbol(comp.ComponentView.AlgebraLabel);

                if (containsPlainName && !containsAlgebraLabel)
                {
                    return new Result<bool>($"There is a custom component in the saved circuit that has a conflicting name with another component ({comp.ComponentView.PlainName})");
                }
                else if (!containsPlainName && containsAlgebraLabel)
                {
                    return new Result<bool>($"There is a custom component in the saved circuit that has a conflicting label with another component ({comp.ComponentView.AlgebraLabel})");
                }
                else if (!containsPlainName && !containsAlgebraLabel)
                {
                    this.AddComponent(comp as CustomComponent);
                }
            }

            return new Result<bool>(true);
        }

        public Result<bool> RemoveComponentByName(string componentName)
        {
            var component = this.Components.FirstOrDefault(comp => comp.ComponentView.PlainName == componentName);
            if (component == null) return new Result<bool>(false);

            TokenTypes.Tokens.Remove(component.ComponentView.AlgebraLabel);
            TokenTypes.OperatorPrecedences.Remove(component.ComponentView.AlgebraLabel);

            this.Components.RemoveAll(comp => comp.ComponentView.PlainName == componentName);

            return this.SaveComponentsRepo();
        }

        public Result<bool> RemoveComponentBySymbol(string componentSymbol)
        {
            var component = this.Components.FirstOrDefault(comp => comp.ComponentView.AlgebraLabel == componentSymbol);
            if (component == null) return new Result<bool>(false);

            TokenTypes.Tokens.Remove(component.ComponentView.AlgebraLabel);
            TokenTypes.OperatorPrecedences.Remove(component.ComponentView.AlgebraLabel);

            this.Components.RemoveAll(comp => comp.ComponentView.AlgebraLabel == componentSymbol);

            return this.SaveComponentsRepo();
        }

        public void UpdateCustomComponentPaths(List<CustomComponent> customComponents)
        {
            foreach (var customComp in customComponents)
            {
                var updatedComp = this.GetComponentByName(customComp.ComponentView.PlainName);
                var updatedFilePath = (updatedComp.ComponentView as CustomComponentView).FilePath;
                customComp.UpdateImageFilePath(updatedFilePath);
            }
        }

        public Result<bool> UpdateImageFilePathByName(string componentName, string newPath)
        {
            var component = this.Components.FirstOrDefault(comp => comp.ComponentView.PlainName == componentName);
            if (component == null) return new Result<bool>(false);

            component.UpdateImageFilePath(newPath);

            return this.SaveComponentsRepo();
        }

        public Result<bool> UpdateImageFilePathBySymbol(string componentSymbol, string newPath)
        {
            var component = this.Components.FirstOrDefault(comp => comp.ComponentView.AlgebraLabel == componentSymbol);
            if (component == null) return new Result<bool>(false);

            component.UpdateImageFilePath(newPath);

            return this.SaveComponentsRepo();
        }

        public bool ContainsComponentWithSymbol(string algebraLabel) => Components.Any(comp => comp.ComponentView.AlgebraLabel == algebraLabel);

        public bool ContainsComponentWithPlainName(string plainName) => Components.Any(comp => comp.ComponentView.PlainName == plainName);

        public IComponent GetComponentByName(string name) => Components.FirstOrDefault(comp => comp.ComponentView.PlainName == name)
                                                                      ?.Clone() as IComponent;

        public IComponent GetComponentBySymbol(string symbol) => Components.FirstOrDefault(comp => comp.ComponentView.AlgebraLabel == symbol)
                                                                          ?.Clone() as IComponent;

        public List<string> RetrieveComponentNames() => Components.Select(comp => comp.ComponentView.PlainName).ToList();

        public List<string> RetrieveComponentSymbols() => Components.Select(comp => comp.ComponentView.AlgebraLabel).ToList();

        public List<IComponent> RetrieveComponents() => Components.Cast<IComponent>().ToList();

        public static string ComponentsRepoFile { get; } = "components.repo";

        public Result<bool> SaveComponentsRepo()
        {
            try
            {
                using (var file = File.Create(ComponentsRepoFile))
                {
                    var serializer = new BinaryFormatter();
                    serializer.Serialize(file, this);
                }

                return new Result<bool>(true);
            }
            catch (IOException ex)
            {
                return new Result<bool>($"There was an error writing the component repository file. Custom components may not be loaded on next startup! ({ex.Message})");
            }
        }

        public static Result<CustomComponentRepository> LoadComponentsRepo()
        {
            if (File.Exists(ComponentsRepoFile))
            {
                try
                {
                    using (var file = File.OpenRead(ComponentsRepoFile))
                    {
                        var deserializer = new BinaryFormatter();
                        var customComponentRepository = (CustomComponentRepository)deserializer.Deserialize(file);

                        var components = customComponentRepository.Components.ToList();

                        customComponentRepository.Components.Clear();
                        components.ForEach(comp => customComponentRepository.AddComponent(comp));

                        return new Result<CustomComponentRepository>(customComponentRepository);
                    }
                }
                catch (IOException ex)
                {
                    return new Result<CustomComponentRepository>($"There was an error reading the components repo file. ({ex.Message})");
                }
            }
            else
            {
                return new Result<CustomComponentRepository>(new CustomComponentRepository());
            }
        }

        private static CustomComponent CreateXorComponent()
        {
            var xorCircuit = new LogicCircuit();

            var inputA = new Input() { Value = true };
            inputA.ComponentView.AlgebraLabel = "A";

            var notGateA = new NotGate();

            notGateA.Connect(inputA);

            var inputB = new Input() { Value = false };
            inputB.ComponentView.AlgebraLabel = "B";

            var notGateB = new NotGate();

            notGateB.Connect(inputB);

            var andGate1 = new AndGate();

            andGate1.Connect(notGateA);
            andGate1.Connect(inputB);

            var andGate2 = new AndGate();

            andGate2.Connect(inputA);
            andGate2.Connect(notGateB);

            var orGate = new OrGate();

            orGate.Connect(andGate1);
            orGate.Connect(andGate2);

            var output = new Output();

            output.Connect(orGate);

            xorCircuit.AddComponent(inputA);
            xorCircuit.AddComponent(inputB);
            xorCircuit.AddComponent(notGateA);
            xorCircuit.AddComponent(notGateB);
            xorCircuit.AddComponent(andGate1);
            xorCircuit.AddComponent(andGate2);
            xorCircuit.AddComponent(orGate);
            xorCircuit.AddComponent(output);

            var xorComponent = new CustomComponent(xorCircuit, "XOR", "@", $"pack://application:,,,/Data/ComponentImages/XOR.png");

            return xorComponent;
        }
    }
}
