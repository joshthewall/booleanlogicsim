﻿using BooleanLogicSim.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSimTests.Model.BooleanExpressions.Data
{
    // (A + B)(A + C)
    public static class ProductOfSums
    {
        public static TreeNode<string> GetProductOfSumsTree()
        {
            var andNode = new TreeNode<string>("*");

            var orNode1 = new TreeNode<string>("+");

            var orNode2 = new TreeNode<string>("+");

            var inputANode = new TreeNode<string>("A");

            var inputBNode = new TreeNode<string>("B");

            var inputCNode = new TreeNode<string>("C");

            andNode.Left = orNode1;
            andNode.Right = orNode2;

            orNode1.Left = inputANode;
            orNode1.Right = inputBNode;

            orNode2.Left = inputANode;
            orNode2.Right = inputCNode;

            return andNode;
        }
    }
}
