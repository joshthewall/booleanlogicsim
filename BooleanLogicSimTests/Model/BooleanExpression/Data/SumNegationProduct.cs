﻿using BooleanLogicSim.Circuit.Logic.Components;
using BooleanLogicSim.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSimTests.Model.BooleanExpressions.Data
{
    // F + ~FV
    public static class SumNegationProduct
    {
        public static TreeNode<string> GetSumNegationProductTree()
        {
            var orNode = new TreeNode<string>("+");

            var notNode = new TreeNode<string>("~");

            var andNode = new TreeNode<string>("*");

            var inputFNode = new TreeNode<string>("F");

            var inputVNode = new TreeNode<string>("V");

            orNode.Left = inputFNode;
            orNode.Right = andNode;

            andNode.Left = notNode;
            andNode.Right = inputVNode;

            notNode.Left = inputFNode;

            return orNode;
        }
    }
}
