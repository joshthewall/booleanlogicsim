﻿using BooleanLogicSim;
using BooleanLogicSim.Circuit.Logic;
using BooleanLogicSim.Data;
using BooleanLogicSim.Model;
using BooleanLogicSim.Model.BooleanAlgebra;
using BooleanLogicSim.Model.LogicCircuits;
using BooleanLogicSimTests.Model.BooleanExpressions.Data;
using BooleanLogicSimTests.Model.LogicCircuits.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BooleanLogicSimTests.Model.BooleanExpressions
{
    [TestClass()]
    public class BooleanExpressionConversionToTreeTests
    {
        private readonly IComponentRepository _componentRepository;

        public BooleanExpressionConversionToTreeTests()
        {
            var targetAssembly = typeof(MainWindow).Assembly;
            Application.ResourceAssembly = targetAssembly;

            _componentRepository = new DefaultComponentRepository(new CustomComponentRepository());
        }

        [TestMethod()]
        public void XOREquivalentExpression_ConvertsCorrectly_ToBinaryExpressionTree()
        {
            var xorEquivalentExpressionResult = BooleanExpression.FromInfixString("~AB + A~B", _componentRepository);
            var xorEquivalentExpression = !xorEquivalentExpressionResult.IsError ? xorEquivalentExpressionResult.Value : null;

            var actualXOREquivalentTreeResult = xorEquivalentExpression.ConvertToTree();
            var actualXOREquivalentTree = !actualXOREquivalentTreeResult.IsError ? actualXOREquivalentTreeResult.Value : null;

            var expectedXOREquivalentTree = ToBooleanExpressionTree(XOREquivalentCircuit.GetExpectedXOREquivalentTree());

            Assert.IsNotNull(xorEquivalentExpression);
            Assert.IsNotNull(actualXOREquivalentTree);
            Assert.IsTrue(TreesAreEquivalent(expectedXOREquivalentTree, actualXOREquivalentTree));
        }

        [TestMethod()]
        public void SumNegationProduct_ConvertsCorrectly_ToBinaryExpressionTree()
        {
            var expressionResult = BooleanExpression.FromInfixString("F + ~FV", _componentRepository);
            var expression = !expressionResult.IsError ? expressionResult.Value : null;

            var actualTreeResult = expression.ConvertToTree();
            var actualTree = !actualTreeResult.IsError ? actualTreeResult.Value : null;

            var expectedTree = SumNegationProduct.GetSumNegationProductTree();

            Assert.IsNotNull(expression);
            Assert.IsNotNull(actualTree);
            Assert.IsTrue(TreesAreEquivalent(expectedTree, actualTree));
        }

        [TestMethod()]
        public void ProductOfSums_ConvertsCorrectly_ToBinaryExpressionTree()
        {
            var expressionResult = BooleanExpression.FromInfixString("(A+B)(A+C)", _componentRepository);
            var expression = !expressionResult.IsError ? expressionResult.Value : null;

            var actualTreeResult = expression.ConvertToTree();
            var actualTree = !actualTreeResult.IsError ? actualTreeResult.Value : null;

            var expectedTree = ProductOfSums.GetProductOfSumsTree();

            Assert.IsNotNull(expression);
            Assert.IsNotNull(actualTree);
            Assert.IsTrue(TreesAreEquivalent(expectedTree, actualTree));
        }

        [TestMethod()]
        public void SingleInput_ConvertsCorrectly_ToBinaryExpressionTree()
        {
            var expressionResult = BooleanExpression.FromInfixString("A", _componentRepository);
            var expression = !expressionResult.IsError ? expressionResult.Value : null;

            var actualTreeResult = expression.ConvertToTree();
            var actualTree = !actualTreeResult.IsError ? actualTreeResult.Value : null;

            var expectedTree = new TreeNode<string>("A");

            Assert.IsNotNull(expression);
            Assert.IsNotNull(actualTree);
            Assert.IsTrue(TreesAreEquivalent(expectedTree, actualTree));
        }

        [TestMethod()]
        public void SingleInputBracketed_ConvertsCorrectly_ToBinaryExpressionTree()
        {
            var expressionResult = BooleanExpression.FromInfixString("((C))", _componentRepository);
            var expression = !expressionResult.IsError ? expressionResult.Value : null;

            var actualTreeResult = expression.ConvertToTree();
            var actualTree = !actualTreeResult.IsError ? actualTreeResult.Value : null;

            var expectedTree = new TreeNode<string>("C");

            Assert.IsNotNull(expression);
            Assert.IsNotNull(actualTree);
            Assert.IsTrue(TreesAreEquivalent(expectedTree, actualTree));
        }

        [TestMethod()]
        public void OnlyBrackets_Produces_ErrorInResult()
        {
            var expressionResult = BooleanExpression.FromInfixString("()", _componentRepository);

            Assert.IsTrue(expressionResult.IsError);
        }

        [TestMethod()]
        public void MissingRightBracket_Produces_ErrorInResult()
        {
            var expressionResult = BooleanExpression.FromInfixString("(A+B", _componentRepository);

            Assert.IsTrue(expressionResult.IsError);
        }

        [TestMethod()]
        public void MissingLeftBracket_Produces_ErrorInResult()
        {
            var expressionResult = BooleanExpression.FromInfixString("A(CD))", _componentRepository);

            Assert.IsTrue(expressionResult.IsError);
        }

        [TestMethod()]
        public void MalformedExpression_Produces_ErrorInResult()
        {
            var expressionResult = BooleanExpression.FromInfixString("~AB+A()", _componentRepository);

            Assert.IsTrue(expressionResult.IsError);
        }

        private bool TreesAreEquivalent(TreeNode<string> expectedTreeNode, TreeNode<string> actualTreeNode)
        {
            if (expectedTreeNode == null && actualTreeNode == null) return true;

            if (expectedTreeNode?.Item != actualTreeNode?.Item) return false;

            return TreesAreEquivalent(expectedTreeNode?.Left, actualTreeNode?.Left) && TreesAreEquivalent(expectedTreeNode?.Right, actualTreeNode?.Right);
        }

        private TreeNode<string> ToBooleanExpressionTree(TreeNode<ICircuitComponent> circuitNode)
        {
            if (circuitNode == null) return null;

            var expressionNode = new TreeNode<string>(circuitNode.Item.ComponentView.AlgebraLabel)
            {
                Left = ToBooleanExpressionTree(circuitNode.Left),
                Right = ToBooleanExpressionTree(circuitNode.Right)
            };

            return expressionNode;
        }
    }
}
