﻿using System;
using System.Windows;
using BooleanLogicSim;
using BooleanLogicSim.Circuit.Logic;
using BooleanLogicSim.Circuit.Logic.Components;
using BooleanLogicSim.Model;
using BooleanLogicSim.Model.LogicCircuits;
using BooleanLogicSimTests.Model.LogicCircuits.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BooleanLogicSimTests.Model.LogicCircuits.Tests
{
    [TestClass()]
    public class LogicCircuitConversionToTreeTests
    {
        public LogicCircuitConversionToTreeTests()
        {
            var targetAssembly = typeof(MainWindow).Assembly;
            Application.ResourceAssembly = targetAssembly;
        }

        [TestMethod()]
        public void XOREquivalentCircuit_ConvertsCorrectly_ToBinaryExpressionTree()
        {
            var xorEquivalentCircuit = XOREquivalentCircuit.GetXOREquivalentCircuit();

            var actualXOREquivalentTreeResult = xorEquivalentCircuit.ConstructBinaryExpressionTree();
            var actualXOREquivalentTree = !actualXOREquivalentTreeResult.IsError ? actualXOREquivalentTreeResult.Value : null;

            var expectedXOREquivalentTree = XOREquivalentCircuit.GetExpectedXOREquivalentTree();

            Assert.IsNotNull(actualXOREquivalentTree);
            Assert.IsTrue(TreesAreEquivalent(expectedXOREquivalentTree, actualXOREquivalentTree));
        }

        [TestMethod()]
        public void SimpleNOTGateCircuit_ConvertsCorrectly_ToBinaryExpressionTree()
        {
            var xorEquivalentCircuit = XOREquivalentCircuit.GetXOREquivalentCircuit();

            var actualXOREquivalentTreeResult = xorEquivalentCircuit.ConstructBinaryExpressionTree();
            var actualXOREquivalentTree = !actualXOREquivalentTreeResult.IsError ? actualXOREquivalentTreeResult.Value : null;

            var expectedXOREquivalentTree = XOREquivalentCircuit.GetExpectedXOREquivalentTree();

            Assert.IsNotNull(actualXOREquivalentTree);
            Assert.IsTrue(TreesAreEquivalent(expectedXOREquivalentTree, actualXOREquivalentTree));

            var notGateCircuit = NOTGateCircuit.GetNOTGateCircuit();

            var actualNOTGateTreeResult = notGateCircuit.ConstructBinaryExpressionTree();
            var actualNOTGateTree = !actualNOTGateTreeResult.IsError ? actualNOTGateTreeResult.Value : null;

            var expectedNOTGateTree = NOTGateCircuit.GetExpectedNOTGateTree();

            Assert.IsNotNull(actualNOTGateTree);
            Assert.IsTrue(TreesAreEquivalent(expectedNOTGateTree, actualNOTGateTree));
        }

        [TestMethod()]
        public void InputOutputCircuit_ConvertsCorrectly_ToBinaryExpressionTree()
        {
            var inputOutputCircuit = InputOutputCircuit.GetInputOutputCircuit();

            var actualInputOutputTreeResult = inputOutputCircuit.ConstructBinaryExpressionTree();
            var actualInputOutputTree = !actualInputOutputTreeResult.IsError ? actualInputOutputTreeResult.Value : null;

            var expectedInputOutputTree = InputOutputCircuit.GetExpectedInputOutputTree();

            Assert.IsNotNull(actualInputOutputTree);
            Assert.IsTrue(TreesAreEquivalent(expectedInputOutputTree, actualInputOutputTree));
        }

        [TestMethod()]
        public void NotFullyConnectedCircuit_Produces_ErrorInResult()
        {
            var notFullyConnectedCircuit = NotFullyConnectedCircuit.GetNotFullyConnectedCircuit();

            var actualNotFullyConnectedTreeResult = notFullyConnectedCircuit.ConstructBinaryExpressionTree();

            Assert.IsTrue(actualNotFullyConnectedTreeResult.IsError);
        }

        [TestMethod()]
        public void NoInputCircuit_Produces_ErrorInResult()
        {
            var noInputCircuit = NoInputCircuit.GetNoInputCircuit();

            var actualNoInputTreeResult = noInputCircuit.ConstructBinaryExpressionTree();

            Assert.IsTrue(actualNoInputTreeResult.IsError);
        }

        [TestMethod()]
        public void NoOutputCircuit_Produces_ErrorInResult()
        {
            var noOutputCircuit = NoOutputCircuit.GetNoOutputCircuit();

            var actualNoOutputTreeResult = noOutputCircuit.ConstructBinaryExpressionTree();

            Assert.IsTrue(actualNoOutputTreeResult.IsError);
        }

        private bool TreesAreEquivalent(TreeNode<ICircuitComponent> expectedTreeNode, TreeNode<ICircuitComponent> actualTreeNode)
        {
            if (expectedTreeNode == null && actualTreeNode == null) return true;

            if (expectedTreeNode?.Item?.GetType()?.Equals(actualTreeNode?.Item?.GetType()) == false)
            {
                return false;
            }

            return TreesAreEquivalent(expectedTreeNode?.Left, actualTreeNode?.Left) && TreesAreEquivalent(expectedTreeNode?.Right, actualTreeNode?.Right);
        }
    }
}
