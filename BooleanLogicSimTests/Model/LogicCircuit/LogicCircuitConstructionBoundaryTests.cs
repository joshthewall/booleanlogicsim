﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BooleanLogicSim.Model.LogicCircuits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BooleanLogicSim.Circuit.Logic.Components;
using BooleanLogicSim.Circuit.Logic;
using System.Reflection;
using System.Windows;

namespace BooleanLogicSim.Model.LogicCircuits.Tests
{
    [TestClass()]
    public class LogicCircuitConstructionBoundaryTests
    {
        private readonly LogicCircuit _logicCircuit;

        public LogicCircuitConstructionBoundaryTests()
        {
            var targetAssembly = typeof(MainWindow).Assembly;
            Application.ResourceAssembly = targetAssembly;

            _logicCircuit = new LogicCircuit();
        }

        [TestMethod()]
        public void NewLogicCircuit_HasNoInputs()
        {
            var expectedEmptyList = new List<Input>();
            CollectionAssert.AreEquivalent(expectedEmptyList, _logicCircuit.Inputs);
        }

        [TestMethod()]
        public void NewLogicCircuit_HasNoInnerComponents()
        {
            var expectedEmptyList = new List<ICircuitComponent>();
            CollectionAssert.AreEquivalent(expectedEmptyList, _logicCircuit.InnerComponents);
        }

        [TestMethod()]
        public void NewLogicCircuit_HasNoOutputComponent()
        {
            Assert.IsNull(_logicCircuit.Output);
        }

        [TestMethod()]
        public void AddSingleInputComponent_ResultsInSingletonInputsList()
        {
            var input = new Input();
            input.ComponentView.AlgebraLabel = "A";

            _logicCircuit.AddComponent(input);

            var expected = new List<Input>() { input };
            CollectionAssert.AreEquivalent(expected, _logicCircuit.Inputs);
        }

        [TestMethod()]
        public void AddDistinctInputComponents_ResultsInListWithAllAddedInputs()
        {
            var input1 = new Input();
            input1.ComponentView.AlgebraLabel = "A";

            var input2 = new Input();
            input1.ComponentView.AlgebraLabel = "B";

            _logicCircuit.AddComponent(input1);
            _logicCircuit.AddComponent(input2);

            var expected = new List<Input>() { input1, input2 };
            CollectionAssert.AreEquivalent(expected, _logicCircuit.Inputs);
        }

        [TestMethod()]
        public void AddRepeatedInputComponents_ResultsInListWithDistinctElements()
        {
            var input = new Input();
            input.ComponentView.AlgebraLabel = "A";

            _logicCircuit.AddComponent(input);
            _logicCircuit.AddComponent(input);

            var expected = new List<Input>() { input };

            CollectionAssert.AreEquivalent(expected, _logicCircuit.Inputs);
        }

        [TestMethod()]
        public void AddInputComponentsWithRepeatedAlgebraLabels_ResultsInListWithOnlyFirstOfInputsAdded()
        {
            var input1 = new Input();
            input1.ComponentView.AlgebraLabel = "A";

            var input2 = new Input();
            input2.ComponentView.AlgebraLabel = "A";

            _logicCircuit.AddComponent(input1);
            _logicCircuit.AddComponent(input2);

            var expected = new List<Input>() { input1 };

            CollectionAssert.AreEquivalent(expected, _logicCircuit.Inputs);
        }

        [TestMethod()]
        public void AddDistinctInnerComponentsOfSameType_ResultsInListWithBothComponentsAdded()
        {
            var andGate1 = new AndGate();
            var andGate2 = new AndGate();

            _logicCircuit.AddComponent(andGate1);
            _logicCircuit.AddComponent(andGate2);

            var expected = new List<ICircuitComponent>() { andGate1, andGate2 };

            CollectionAssert.AreEquivalent(expected, _logicCircuit.InnerComponents);
        }

        [TestMethod()]
        public void AddDistinctInnerComponentsOfDifferentType_ResultsInListWithBothComponentsAdded()
        {
            var andGate = new AndGate();
            var orGate = new OrGate();

            _logicCircuit.AddComponent(andGate);
            _logicCircuit.AddComponent(orGate);

            var expected = new List<ICircuitComponent>() { andGate, orGate };

            CollectionAssert.AreEquivalent(expected, _logicCircuit.InnerComponents);
        }

        [TestMethod()]
        public void AddRepeatedInnerComponents_ResultsInListWithDistinctElements()
        {
            var andGate = new AndGate();

            _logicCircuit.AddComponent(andGate);

            _logicCircuit.AddComponent(andGate);

            var expected = new List<ICircuitComponent>() { andGate };

            CollectionAssert.AreEquivalent(expected, _logicCircuit.InnerComponents);
        }

        [TestMethod()]
        public void AddSingleOutputComponent_ResultsInNonNullOutput()
        {
            var output = new Output();

            _logicCircuit.AddComponent(output);

            Assert.IsNotNull(_logicCircuit.Output);
        }

        [TestMethod()]
        public void AddDistinctOutputComponents_ResultsInFirstOutputSet()
        {
            var output1 = new Output();
            var output2 = new Output();

            _logicCircuit.AddComponent(output1);
            _logicCircuit.AddComponent(output2);

            Assert.AreEqual(output1, _logicCircuit.Output);
        }

        [TestMethod()]
        public void AddRepeatedOutputComponent_ResultsInOutputStillSet()
        {
            var output = new Output();

            _logicCircuit.AddComponent(output);
            _logicCircuit.AddComponent(output);

            Assert.AreEqual(output, _logicCircuit.Output);
        }
    }
}