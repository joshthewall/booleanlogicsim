﻿using BooleanLogicSim.Circuit.Logic.Components;
using BooleanLogicSim.Model.LogicCircuits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSimTests.Model.LogicCircuits.Data
{
    public static class NotFullyConnectedCircuit
    {
        public static LogicCircuit GetNotFullyConnectedCircuit()
        {
            var notFullyConnectedCircuit = new LogicCircuit();

            var inputA = new Input() { Value = true };
            inputA.ComponentView.AlgebraLabel = "A";

            var notGate = new NotGate();

            notGate.Connect(inputA);

            var output = new Output();

            // No connection from notGate to output

            notFullyConnectedCircuit.AddComponent(inputA);
            notFullyConnectedCircuit.AddComponent(notGate);
            notFullyConnectedCircuit.AddComponent(output);

            return notFullyConnectedCircuit;
        }
    }
}
