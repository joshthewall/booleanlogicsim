﻿using BooleanLogicSim.Circuit.Logic;
using BooleanLogicSim.Circuit.Logic.Components;
using BooleanLogicSim.Model;
using BooleanLogicSim.Model.LogicCircuits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSimTests.Model.LogicCircuits.Data
{
    public static class NOTGateCircuit
    {
        public static LogicCircuit GetNOTGateCircuit()
        {
            var notGateCircuit = new LogicCircuit();

            var inputA = new Input() { Value = true };
            inputA.ComponentView.AlgebraLabel = "A";

            var notGate = new NotGate();

            notGate.Connect(inputA);

            var output = new Output();

            output.Connect(notGate);

            notGateCircuit.AddComponent(inputA);
            notGateCircuit.AddComponent(notGate);
            notGateCircuit.AddComponent(output);

            return notGateCircuit;
        }

        public static TreeNode<ICircuitComponent> GetExpectedNOTGateTree()
        {
            var notGateNode = new TreeNode<ICircuitComponent>(new NotGate());

            var input = new Input();
            input.ComponentView.AlgebraLabel = "A";

            var inputNode = new TreeNode<ICircuitComponent>(input);

            notGateNode.Left = inputNode;

            return notGateNode;
        }
    }
}
