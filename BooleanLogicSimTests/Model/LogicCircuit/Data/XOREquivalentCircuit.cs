﻿using BooleanLogicSim.Circuit.Logic;
using BooleanLogicSim.Circuit.Logic.Components;
using BooleanLogicSim.Model;
using BooleanLogicSim.Model.LogicCircuits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSimTests.Model.LogicCircuits.Data
{
    public static class XOREquivalentCircuit
    {
        public static LogicCircuit GetXOREquivalentCircuit()
        {
            var xorCircuit = new LogicCircuit();

            var inputA = new Input() { Value = true };
            inputA.ComponentView.AlgebraLabel = "A";

            var notGateA = new NotGate();

            notGateA.Connect(inputA);

            var inputB = new Input() { Value = false };
            inputB.ComponentView.AlgebraLabel = "B";

            var notGateB = new NotGate();

            notGateB.Connect(inputB);

            var andGate1 = new AndGate();

            andGate1.Connect(notGateA);
            andGate1.Connect(inputB);

            var andGate2 = new AndGate();

            andGate2.Connect(inputA);
            andGate2.Connect(notGateB);

            var orGate = new OrGate();

            orGate.Connect(andGate1);
            orGate.Connect(andGate2);

            var output = new Output();

            output.Connect(orGate);

            xorCircuit.AddComponent(inputA);
            xorCircuit.AddComponent(inputB);
            xorCircuit.AddComponent(notGateA);
            xorCircuit.AddComponent(notGateB);
            xorCircuit.AddComponent(andGate1);
            xorCircuit.AddComponent(andGate2);
            xorCircuit.AddComponent(orGate);
            xorCircuit.AddComponent(output);

            return xorCircuit;
        }

        public static TreeNode<ICircuitComponent> GetExpectedXOREquivalentTree()
        {
            var orGateNode = new TreeNode<ICircuitComponent>(new OrGate());

            var andGate1 = new TreeNode<ICircuitComponent>(new AndGate());

            var andGate2 = new TreeNode<ICircuitComponent>(new AndGate());

            orGateNode.Left = andGate1;
            orGateNode.Right = andGate2;

            var notGateA = new TreeNode<ICircuitComponent>(new NotGate());

            var notGateB = new TreeNode<ICircuitComponent>(new NotGate());

            var inputAItem = new Input();
            inputAItem.ComponentView.AlgebraLabel = "A";

            var inputBItem = new Input();
            inputBItem.ComponentView.AlgebraLabel = "B";

            var inputA = new TreeNode<ICircuitComponent>(inputAItem);

            var inputB = new TreeNode<ICircuitComponent>(inputBItem);

            notGateA.Left = inputA;
            notGateB.Left = inputB;

            andGate1.Left = notGateA;
            andGate1.Right = inputB;

            andGate2.Left = inputA;
            andGate2.Right = notGateB;

            return orGateNode;
        }
    }
}
