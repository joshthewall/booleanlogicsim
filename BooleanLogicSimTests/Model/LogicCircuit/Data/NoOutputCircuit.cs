﻿using BooleanLogicSim.Circuit.Logic.Components;
using BooleanLogicSim.Model.LogicCircuits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSimTests.Model.LogicCircuits.Data
{
    public static class NoOutputCircuit
    {
        public static LogicCircuit GetNoOutputCircuit()
        {
            var noOutputCircuit = new LogicCircuit();

            var inputA = new Input() { Value = true };
            inputA.ComponentView.AlgebraLabel = "A";

            var notGate = new NotGate();

            notGate.Connect(inputA);

            noOutputCircuit.AddComponent(inputA);
            noOutputCircuit.AddComponent(notGate);

            return noOutputCircuit;
        }
    }
}
