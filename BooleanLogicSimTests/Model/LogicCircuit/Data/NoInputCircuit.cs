﻿using BooleanLogicSim.Circuit.Logic.Components;
using BooleanLogicSim.Model.LogicCircuits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSimTests.Model.LogicCircuits.Data
{
    public static class NoInputCircuit
    {
        public static LogicCircuit GetNoInputCircuit()
        {
            var noInputCircuit = new LogicCircuit();

            var notGate = new NotGate();

            var output = new Output();

            output.Connect(notGate);

            noInputCircuit.AddComponent(notGate);
            noInputCircuit.AddComponent(output);

            return noInputCircuit;
        }
    }
}
