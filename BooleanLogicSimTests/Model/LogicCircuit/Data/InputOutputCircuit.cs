﻿using BooleanLogicSim.Circuit.Logic;
using BooleanLogicSim.Circuit.Logic.Components;
using BooleanLogicSim.Model;
using BooleanLogicSim.Model.LogicCircuits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooleanLogicSimTests.Model.LogicCircuits.Data
{
    public static class InputOutputCircuit
    {
        public static LogicCircuit GetInputOutputCircuit()
        {
            var inputOutputCircuit = new LogicCircuit();

            var inputA = new Input() { Value = true };
            inputA.ComponentView.AlgebraLabel = "A";

            var output = new Output();

            output.Connect(inputA);

            inputOutputCircuit.AddComponent(inputA);
            inputOutputCircuit.AddComponent(output);

            return inputOutputCircuit;
        }

        public static TreeNode<ICircuitComponent> GetExpectedInputOutputTree()
        {
            var input = new Input();
            input.ComponentView.AlgebraLabel = "A";

            var inputNode = new TreeNode<ICircuitComponent>(input);

            return inputNode;
        }
    }
}
